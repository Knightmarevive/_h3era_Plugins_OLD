#include <windows.h>
#include "Era.h"
  
using namespace Era;
  
const int ADV_MAP   = 37;
const int CTRL_LMB  = 4;
const int LMB_PUSH  = 12;

void (*ConfigureArt)(int target, char* buf);

void __stdcall  link_emerald(TEvent* e)
{
	//HMODULE Amethyst = LoadLibraryA("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll");
	HMODULE Emerald = 0;
	//DWORD flags = 0; // GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS;
	//::GetModuleHandleEx(flags, reinterpret_cast<LPCTSTR>("Mods\\Knightmare Kingdoms\\eraplugins\\amethyst2.3.dll"), &Amethyst);
	Emerald = GetModuleHandleA("emerald3_3.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_4.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_5.era");
	if (!Emerald) Emerald = GetModuleHandleA("emerald3_6.era");

	if (Emerald) {
		ConfigureArt = (void(*)(int, char*)) GetProcAddress(Emerald, "ConfigureArt");
	}
	else { MessageBoxA(0, "couldn't link to Emerald", "Knightmare Text Plugin", MB_OK); }
}


void __stdcall fix_arts(TEvent* e) {
	if (!ConfigureArt) {
		MessageBoxA(0, "general error", "Knightmare Text Plugin", MB_OK);
		return;
	}
	ConfigureArt(166,const_cast<char*>( "Name=\"Broach of Wealth (166)\" Description=\"{Broach of Wealth} \n\
																									\n\
		Gives 3 mithril and 9 ore and 9 wood and 7500 gold daily.\""));
	
	ConfigureArt(168, const_cast<char*>("Name=\"Surcoat of Lloth (168)\" \
	Description=\"{ Surcoat of Lloth } \n\
		\n\
		Gives each owned creature : \n\
		>> all shield spells\n\
		>> all protection from elements\n\
		>> magic mirror\n\
		>> 50 % chance for 50 % deflect\""));

	ConfigureArt(170, const_cast<char*>("Name=\"Horn of Summoning (170)\" Description=\"{Horn of Summoning} \n\
\n\
		Summons mighty Town Guardians to protect hero, even if not in siege\""));

	ConfigureArt(169, const_cast<char*>("Name=\"Boots of Rincewind (169)\" Description=\"{Boots of Rincewind (169)}\n\
		\n\
		Gives all your creatures chance to rebirthand gives 3 additional spells for caster creatures.\""));

	ConfigureArt(165, const_cast<char*>("Name=\"Ring of Slavery (165)\" Description=\"{Ring of Slavery (165)} \n\
		\n\
		Gives all your creatures : \n\
		>> Strike and Return\n\
		>> No Retailation\n\
		>> More Retailations\""));

	ConfigureArt(167, const_cast<char*>("Name=\"Armor of Witchcraft (167) \" Description=\"{Armor of Witchcraft}\n\
		\n\
		Gives all your creatures :\n\
		>> Attack All Around\n\
		>> No Enemy Retaliation\n\
		>> Fearless\n\" "));

	ConfigureArt(162, const_cast<char*>("Name=\"Sword of Deepest Knightmare (162)\" Description=\"{Sword of Deepest Knightmare}\n\
		\n\
		Gives all your creatures :\n\
		>> Immune to Direct Damage spells\n\
		>> 13 additional spells for caster creatures\n\
		>> Clone(each creature once at beggining of battle)\n\" "));

	ConfigureArt(161, const_cast<char*>("Name=\"Helmet of Vision (161)\" Description=\"{Helmet of Vision (161)}\n\
		\n\
		>> Gives additional 9 Scouting Radius\n\
		>> Gives 22 Experience per step\n\
		>> Doubles Scouting Encounter Chance\n\" "));

	ConfigureArt(485, const_cast<char*>("Name=\"Dracopedia\" Description=\"{Dracopedia}\n\
		\n\
		Gives All Your Creatures :\n\
		>> Fly\n\
		>> Dragon Nature\n\
		>> Immune to Fire\n\
		>> Advanced Inferno After Attack\n\" "));

	ConfigureArt( 108, const_cast<char*>("Name=\"Pendant of Fanatical Courage\" Description=\"{Pendant of Fanatical Courage}\n\
		\n\
		>> Gives Max Morale And Luck\n\
		>> Mind Spell Immunity\n\
		>> Deepest Sorrowand Misfortune to your enemies\n\
		>> Doubles Your Inspiration Bonus\n\" "));

	ConfigureArt(415, const_cast<char*>("Name=\"The Garb of the Forest Lord\" Description=\"{The Garb of the Forest Lord}\n\
\n\
		Gives 2 Sylvan Dragons, 0 Sylfaen Dragon each week\n\
		Gives Something Else.\n\" "));

	ConfigureArt(164, const_cast<char*>("Name=\"Horned Ring\" Description=\"{Horned Ring}\n\
		Triples efficiency of exorcism.\n\" "));

	ConfigureArt(486, const_cast<char*>("Name=\"Bard's Mandola\" Description=\"{Bard's Mandola}\n\
\n\
		Reduces Monster Aggression by 5 Altogether.\n\
		Gives Something Else.\n\" "));

	ConfigureArt(156, const_cast<char*>("Name=\"{Warlord's Banner}\" Description=\"{Warlord's Banner}\n\n\
		With stack experience enabled, the Warlord's Banner gives additional bonuses in combat. To equip it, drag it to the troop stack of your choice.\n\n\
		Choose a bonus by right-clicking on the Banner in the stack's experience screen. You can change the bonus any time except in combat.\n\n\
		Equipped on {Misc Slot} gives 25 XP Daily\" "));

	ConfigureArt(470, const_cast<char*>("Name=\"{Ancient Book} Tier One #470\" Description=\" {Ancient Book} #470 \n\nTier One\n\n Gives 175 XP Daily\" "));
	ConfigureArt(502, const_cast<char*>("Name=\"{Ancient Book} Tier Two #502\" Description=\" {Ancient Book} #502 \n\nTier Two\n\n Gives 1225 XP Daily\" "));
	ConfigureArt(471, const_cast<char*>("Name=\"{Ancient Book} Tier Three #471\" Description=\" {Ancient Book} #471 \n\nTier Three\n\n  Gives 8575 XP Daily\" "));
	ConfigureArt(483, const_cast<char*>("Name=\"{Ethernal Gem} Tier Max #483\" Description=\" {Ethernal Gem} #483 \n\nTier Max\n\n Gives 60025 XP Daily\" "));


	ConfigureArt(491, const_cast<char*>("Name=\"{Ring of Knightmare} #491\" Description=\" {Ring of Knightmare} #491\" "));
	ConfigureArt(543, const_cast<char*>("Name=\"{Scarab Pendant} #543\" Description=\" {Scarab Pendant} #543\" "));
	ConfigureArt(574, const_cast<char*>("Name=\"{Enchanted Bow} #574\" Description=\" {Enchanted Bow} #574\" "));


	ConfigureArt(420, const_cast<char*>("Name=\"{The Blitz Cloak} #420\" Description=\" {The Blitz Cloak} #420 \n\" "));
	ConfigureArt(141, const_cast<char*>("Name=\"{Magic Wand} #141\" Description=\" {Magic Wand} #141 \n\n\
		+ 7 Power \n+ 5 Cats \n+ Auto SPell Magic Mirror\" "));
	ConfigureArt(155, const_cast<char*>("Name=\"{Ring of Slava} #155\" Description=\" {Ring of Slava} #155 \n\n\
		\n+ 2 Attack\n+ 2 Knowledge\n+ 2 Power \n+ 2 Knowledge \n+ 5 Cats \n+ 2 Shots\" "));
	ConfigureArt(126, const_cast<char*>("Name=\"{Orb of Inhibition} #126\" Description=\" {Orb of Inhibition} #126 \n\n\
		+ 2 Knowledge\n+ 2 Power \n+ 3 casts \n Auto Spell Antimagic\" "));

	// ConfigureArt( 1000, const_cast<char*>("Name=\"art name \" Description=\" description \n\" "));

}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain (HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
  if (reason == DLL_PROCESS_ATTACH)
  {
    ConnectEra();
    //RegisterHandler(OnAdventureMapLeftMouseClick, "OnAdventureMapLeftMouseClick");
    //ApiHook((void*) Hook_BattleMouseHint, HOOKTYPE_BRIDGE, (void*) 0x74fd1e);
	RegisterHandler(link_emerald, "OnSavegameRead");
	RegisterHandler(link_emerald, "OnBeforeErmInstructions");
	RegisterHandler(fix_arts, "OnOpenHeroScreen");
  }
  return TRUE;
};
