// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#include "citrine.h"

GAMEDATA save;
Patcher * globalPatcher;
PatcherInstance *citrine;

extern void __stdcall Citrine(PEvent e);



void __stdcall InitData (PEvent e)
{

}


void __stdcall StoreData (PEvent e)
{
	WriteSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
}


void __stdcall RestoreData (PEvent e)
{
	ReadSavegameSection(sizeof(save), (void*)&save, PINSTANCE_MAIN);
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		//���� ���, ���� �������



		globalPatcher = GetPatcher();
		citrine =  globalPatcher->CreateInstance(PINSTANCE_MAIN);

		ConnectEra();

		//Storing data
		RegisterHandler(InitData, "OnAfterErmInstructions");
		RegisterHandler(StoreData, "OnSavegameWrite");
		RegisterHandler(RestoreData, "OnSavegameRead");




		//������ ��������� ������ ��� ��������		
		//RegisterHandler(ReallocProhibitionTables, "OnAfterCreateWindow");


		//RegisterHandler(LoadConfigs, "OnAfterCreateWindow");

		//�������� �������
		RegisterHandler(Citrine, "OnAfterWoG");


		//Citrine(0);
	}
	return TRUE;
}

