// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#include <windows.h>
#include <stdio.h>
#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"


Patcher * globalPatcher;
PatcherInstance *patcher;

extern "C" __declspec(dllexport) void SetDate(short day, short week, short month)
{
	*(short*)(0x1F63E+*(int*)0x699538) = day;
	*(short*)(0x1F640+*(int*)0x699538) = week;
	*(short*)(0x1F642+*(int*)0x699538) = month;
}



void* __fastcall BuildAndLoadNewDayDef(void* _this, int edx, int posX, int posY, int sizeX, int sizeY, int itemId, char *defname, int cadre, int group, int mirror, int closeDialog, int flags)
{
	short day =		*(short*)(0x1F63E+*(int*)0x699538);
	short week =	*(short*)(0x1F640+*(int*)0x699538);
	short month =	*(short*)(0x1F642+*(int*)0x699538);

	strncpy(ErmZ[1],defname,512);

	EventParams[0]=day;
	EventParams[1]=week;
	EventParams[2]=month;
	FireEvent("OnNewDayAnimation",(void*)(ErmZ+1),4);

	ErmX[1]=day;
	ErmX[2]=week;
	ErmX[3]=month;
	FireErmEvent(4074218);


	return CALL_12(void*, __thiscall, 0x4EA800,_this, posX, posY, sizeX, sizeY, itemId, ErmZ[1], cadre, group, mirror, closeDialog, flags);

}

/*void __stdcall Testt (PEvent e)
{
	if (EventParams[1]==4 && EventParams[2]==6)
		strcpy((char*)(e->Data),"midsumm.def");

	//hprintf(0x69D800,"%i %i %i %s",EventParams[0],EventParams[1],EventParams[2],e->Data);
}*/

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		//���� ���, ���� �������
		globalPatcher = GetPatcher();
		patcher =  globalPatcher->CreateInstance("malachite");
		ConnectEra();
		
		patcher->WriteHiHook(0x450CB7,CALL_,DIRECT_,THISCALL_,(void*)BuildAndLoadNewDayDef);

		//RegisterHandler(Testt,"OnNewDayAnimation");
	}
	return TRUE;
}



