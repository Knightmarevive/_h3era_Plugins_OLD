// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#include <windows.h>
#include <stdio.h>
#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"


Patcher * globalPatcher;
PatcherInstance *patcher;

extern "C" __declspec(dllexport) void SetAdditionalMonster(int castle_id, int type, int amount)
{
	CASTLE *cstl = ((CASTLE*)(*(int*)((*(int*)0x699538) + 0x21614) + sizeof(CASTLE) * castle_id));
	cstl->EightMonsterAmount = amount;
	cstl->EightMonsterType = type;
}

extern "C" __declspec(dllexport) int GetAdditionalMonsterType(int castle_id)
{
	CASTLE *cstl = ((CASTLE*)(*(int*)((*(int*)0x699538) + 0x21614) + sizeof(CASTLE) * castle_id));
	return cstl->EightMonsterType;
}

extern "C" __declspec(dllexport) int GetAdditionalMonsterAmount(int castle_id)
{
	CASTLE *cstl = ((CASTLE*)(*(int*)((*(int*)0x699538) + 0x21614) + sizeof(CASTLE) * castle_id));
	return cstl->EightMonsterAmount;
}


int IsEighthSlotAvailable(CASTLE* cstl)
{
	int ret = false;

	if (cstl->Type == 5 && cstl->Built[2]&64)
		ret = true;

	EventParams[0]=cstl->Number;
    EventParams[1]=(int)cstl;
    EventParams[2]=(int)&ret;
    FireEvent("OnAdditionalMonsterAvailable",0,0);

    ErmX[1]=cstl->Number;
    ErmX[2]=(int)cstl;
    ErmX[3]=(int)&ret;
    FireErmEvent(4074810);

	return ret; 
}


int __stdcall hook_5BDCBF(LoHook* h, HookContext* c)
{
	CASTLE *cstl = (CASTLE*)c->ecx;

	
    EventParams[0]=cstl->Number;
    EventParams[1]=(int)cstl;
    EventParams[2]=0x3C+(int)cstl;
    EventParams[3]=0x40+(int)cstl;
    FireEvent("OnAdditionalMonsterSetting",0,0);

    ErmX[1]=cstl->Number;
    ErmX[2]=(int)cstl;
    ErmX[3]=0x3C+(int)cstl;
    ErmX[4]=0x40+(int)cstl;
    FireErmEvent(4074811);

	return EXEC_DEFAULT;
}


int __stdcall hook_4C8B39(LoHook* h, HookContext* c)
{
	CASTLE *cstl = (CASTLE*)c->ecx;
	
	if (!IsEighthSlotAvailable(cstl))
		c->return_address = 0x4C8B62;
	else
		c->return_address = 0x4C8B5D;


	return NO_EXEC_DEFAULT;
}

int __stdcall hook_51D2AE(LoHook* h, HookContext* c)
{
	CASTLE *cstl = (CASTLE*)c->esi;

	if (!IsEighthSlotAvailable(cstl))
		c->return_address = 0x51E79B;
	else
		c->return_address = 0x51D2DB;
	return NO_EXEC_DEFAULT;
}

int __stdcall hook_5C651F(LoHook* h, HookContext* c)
{
	CASTLE *cstl = (CASTLE*)c->esi;

	if (!IsEighthSlotAvailable(cstl))
		c->return_address = 0x5C66D9;
	else
		c->return_address = 0x5C654D;
	return NO_EXEC_DEFAULT;
}

int __stdcall hook_5D8BD8(LoHook* h, HookContext* c)
{
	CASTLE *cstl = (CASTLE*)c->ecx;

	if (!IsEighthSlotAvailable(cstl))
		c->return_address = 0x5D8C08;
	else
		c->return_address = 0x5D8BFE;
	return NO_EXEC_DEFAULT;
}

int __stdcall hook_5D8C11(LoHook* h, HookContext* c)
{
	CASTLE *cstl = (CASTLE*)c->edx;

	if (!IsEighthSlotAvailable(cstl))
		c->return_address = 0x5D8C95;
	else
		c->return_address = 0x5D8C37;
	return NO_EXEC_DEFAULT;
}



BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		//���� ���, ���� �������
		globalPatcher = GetPatcher();
		patcher =  globalPatcher->CreateInstance("obsidian");
		ConnectEra();
		
		patcher->WriteLoHook(0x4C8B39,(void*)hook_4C8B39); //JNE SHORT 004C8B62
		patcher->WriteLoHook(0x51D2AE,(void*)hook_51D2AE); //JNE 0051E79B
		patcher->WriteLoHook(0x5C651F,(void*)hook_5C651F); //JNE 005C66D9
		patcher->WriteLoHook(0x5D8BD8,(void*)hook_5D8BD8); //JNE SHORT 005D8C08
		patcher->WriteLoHook(0x5D8C11,(void*)hook_5D8C11); //JNE SHORT 005D8C08

		patcher->WriteLoHook(0x5BDCBB,(void*)hook_5BDCBF); //����� ������ � ����� ����
		

		//RegisterHandler(TimerTest,"OnGlobalTimer");
	}
	return TRUE;
}



