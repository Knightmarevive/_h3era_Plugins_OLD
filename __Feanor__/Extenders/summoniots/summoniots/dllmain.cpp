// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "stdafx.h"


Patcher * globalPatcher;
PatcherInstance *citrine;

int __stdcall SummonHook(HiHook* h, _BattleMgr_ *combatman, int spell, int creature, int spellpower, int unk)
{
	int flag = 1;
	((int*)0x91DA34)[1] = (int)(&flag);
	((int*)0x91DA34)[2] = (int)(&spell);
	((int*)0x91DA34)[3] = (int)(&creature);
	((int*)0x91DA34)[4] = (int)(&spellpower);
	((int*)0x91DA34)[5] = (int)(&unk);
	((int*)0x91DA34)[6] = (int)combatman+combatman->current_side*4+0x132C0;

	CallERM(4074520);

	return flag?CALL_5(int, __thiscall, h->GetDefaultFunc(), (int)combatman, spell, creature, spellpower, unk):-1;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		globalPatcher = GetPatcher();
		citrine =  globalPatcher->CreateInstance("summon_elems");

		citrine->WriteHiHook(0x5A7390, SPLICE_, EXTENDED_, THISCALL_, (void*)SummonHook);
		citrine->WriteByte(0x59F887,0xEB);
		citrine->WriteWord(0x5A96D0, 0x9090);		
	}
	return TRUE;
}

