// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.

#include "stdio.h"
#include "stdlib.h"
#include "windows.h"
#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"


Patcher * globalPatcher;
PatcherInstance *emerald;


int __stdcall NewHasArtifact(HiHook* h, HERO* hero, int art)
{
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, art);
	if (h->GetReturnAddress() < 0x700000 && h->GetReturnAddress() > 0x400000) //������� ������ SoD;
	{
		
		if (art==ARTIFACT_ANGELWINGS)
		{
			int icarus = true;
			for(int i=0; i!=7; i++)
			{
				if (hero->Ct[i]!=-1)
					icarus&= GetMonsterRecord(hero->Ct[i])->flags >> 1 & 1;
			}
			ret|=icarus;
		}
	}
	return ret;
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		globalPatcher = GetPatcher();
		emerald =  globalPatcher->CreateInstance("icarus");
		ConnectEra();

		emerald->WriteHiHook(0x4D9460, SPLICE_, EXTENDED_, THISCALL_, (void*)NewHasArtifact);
	}
	return TRUE;
}

