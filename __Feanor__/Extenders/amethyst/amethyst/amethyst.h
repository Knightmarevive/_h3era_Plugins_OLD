#pragma once

         
#include <windows.h>
#include "targetver.h"
#define WIN32_LEAN_AND_MEAN    

#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"


#define PINSTANCE_MAIN "amethyst"


#define OLD_MONSTERS_AMOUNT 197
#define NEW_MONSTERS_AMOUNT 1000
//Savegame
typedef struct
{

}GAMEDATA;

extern GAMEDATA save;
extern Patcher * globalPatcher;
extern PatcherInstance * amethyst;


extern ART_RECORD newtable[NEW_ARTS_AMOUNT];
extern ART_BONUS newbtable[NEW_ARTS_AMOUNT];

extern MONSTER_PROP new_monsters[NEW_MONSTERS_AMOUNT];
extern CRANIM new_cranim[NEW_MONSTERS_AMOUNT];

extern char *names[NEW_MONSTERS_AMOUNT];
extern char *names_pl[NEW_MONSTERS_AMOUNT];
extern char *descripts[NEW_MONSTERS_AMOUNT];
extern char experience_table[MONSTERS_AMOUNT*340];

//bonuses


//events

