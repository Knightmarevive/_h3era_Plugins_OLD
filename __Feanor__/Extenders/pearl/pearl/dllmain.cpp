// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "windows.h"
#include "..\..\include\era.h"
#include "..\..\include\heroes.h"
#include "..\..\include\patcher_x86_commented.hpp"
//#include "..\..\include\HotA\HoMM3.h"

#define PINSTANCE_MAIN "pearl"


Patcher * globalPatcher;
PatcherInstance * pearl;

void InitWoGSpecials(HERO *currhero)
{
	*(int*)0x27F9970 = (int)currhero;
	ErmV[998] = currhero->x;
	ErmV[999] = currhero->y;
	ErmV[1000] = currhero->l;
}




int __stdcall GetNecromancyCreature(HiHook* h, HERO *hero)
{
	int ret = CALL_1(int, __thiscall, h->GetDefaultFunc(), hero);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	InitWoGSpecials(hero);
	CallERM(4074010);


	return ret;
}

float __stdcall GetNecromancyLevel(HiHook* h, HERO *hero, bool allow_overflow)
{
	float ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, allow_overflow);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;
	ErmX[4] = (int)allow_overflow;

	InitWoGSpecials(hero);
	CallERM(4074011);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}

int __stdcall GetManaRestoredPoints(HiHook* h, HERO *hero)
{
	int ret = CALL_1(int, __thiscall, h->GetDefaultFunc(), hero);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	InitWoGSpecials(hero);
	CallERM(4074012);


	return ret;
}

int __stdcall GetScoutingRadius(HiHook* h, HERO *hero)
{
	int ret = CALL_1(int, __thiscall, h->GetDefaultFunc(), hero);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	InitWoGSpecials(hero);
	CallERM(4074013);


	return ret;
}

int __stdcall GetDailyIncome(HiHook* h, HERO *hero)
{
	int ret = CALL_1(int, __thiscall, h->GetDefaultFunc(), hero);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	InitWoGSpecials(hero);
	CallERM(4074014);


	return ret;
}

int __stdcall GetSpellDuration(HiHook* h, HERO *hero)
{
	int ret = CALL_1(int, __thiscall, h->GetDefaultFunc(), hero);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	InitWoGSpecials(hero);
	CallERM(4074015);


	return ret;
}


float __stdcall GetOffenceLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074016);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}


float __stdcall GetEagleEyeLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074017);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}


float __stdcall GetDiplomacyLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074018);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}


float __stdcall GetResistanceLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074019);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}


float __stdcall GetLearningLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);

	
	//hprintf(0x69D800,"ret %f retx100 %f ret*100 %f", ret, ret_x100,(int)(ret * 100));
	CallERM(4074020);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	//hprintf(0x69D800,"ret %f retx100 %f ret*100 %f", ret, ret_x100,(int)(ret * 100));

	return ret;
}


float __stdcall GetIntelligenceLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074021);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}


float __stdcall GetFirstAidLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074022);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}

float __stdcall GetArcheryLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074023);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}

float __stdcall GetArmorerLevel(HiHook* h, HERO *hero)
{
	float ret = CALL_1(float, __thiscall, h->GetDefaultFunc(), hero);
	int ret_x100 = (int)(ret * 100);

	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret_x100);
	ErmX[3] = (int)hero;

	InitWoGSpecials(hero);
	CallERM(4074024);

	if((int)(ret * 100) != ret_x100) ret=ret_x100/100.0;

	return ret;
}


int __stdcall GetHeroMovement(HiHook* h, HERO *hero, int on_sea)
{
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, on_sea);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	ErmX[4] = on_sea;
	InitWoGSpecials(hero);
	CallERM(4074025);
	return ret;
}

int __stdcall GetSpellLevel(HiHook* h, HERO *hero, int spell, int MagicScool)
{
	int ret = CALL_3(int, __thiscall, h->GetDefaultFunc(), hero, spell, MagicScool);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	ErmX[4] = spell;
	ErmX[5] = MagicScool;
	InitWoGSpecials(hero);
	CallERM(4074026);
	return ret;
}

int __stdcall GetSpellCost(HiHook* h, HERO *hero, int spell, void *MonArr, int MagicScool)
{
	int ret = CALL_4(int, __thiscall, h->GetDefaultFunc(), hero, spell, MonArr, MagicScool);
	
	ErmX[1] = hero->Number;
	ErmX[2] = (int)(&ret);
	ErmX[3] = (int)hero;
	ErmX[4] = spell;
	ErmX[5] = (int)MonArr;
	ErmX[6] = MagicScool;
	InitWoGSpecials(hero);
	CallERM(4074027);
	return ret;
}


BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if(ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		globalPatcher = GetPatcher();
		pearl =  globalPatcher->CreateInstance(PINSTANCE_MAIN);
		ConnectEra();

		pearl->WriteHiHook(0x4E3ED0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetNecromancyCreature);
		pearl->WriteHiHook(0x4E3F40,SPLICE_,EXTENDED_,THISCALL_,(void*)GetNecromancyLevel);

		pearl->WriteHiHook(0x4E41B0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetManaRestoredPoints);
		pearl->WriteHiHook(0x4E42E0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetScoutingRadius);
		pearl->WriteHiHook(0x4E4600,SPLICE_,EXTENDED_,THISCALL_,(void*)GetDailyIncome);
		pearl->WriteHiHook(0x4E5020,SPLICE_,EXTENDED_,THISCALL_,(void*)GetSpellDuration);

		pearl->WriteHiHook(0x4E43D0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetArcheryLevel);
		pearl->WriteHiHook(0x4E4520,SPLICE_,EXTENDED_,THISCALL_,(void*)GetOffenceLevel);
		pearl->WriteHiHook(0x4E4580,SPLICE_,EXTENDED_,THISCALL_,(void*)GetArmorerLevel);
		pearl->WriteHiHook(0x4E4690,SPLICE_,EXTENDED_,THISCALL_,(void*)GetEagleEyeLevel);
		pearl->WriteHiHook(0x4E47F0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetDiplomacyLevel);
		pearl->WriteHiHook(0x4E4950,SPLICE_,EXTENDED_,THISCALL_,(void*)GetResistanceLevel);
		pearl->WriteHiHook(0x4E4AB0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetLearningLevel);
		pearl->WriteHiHook(0x4E4B20,SPLICE_,EXTENDED_,THISCALL_,(void*)GetIntelligenceLevel);
		pearl->WriteHiHook(0x4E4B90,SPLICE_,EXTENDED_,THISCALL_,(void*)GetFirstAidLevel);

		pearl->WriteHiHook(0x4E4C00,SPLICE_,EXTENDED_,THISCALL_,(void*)GetHeroMovement);

		pearl->WriteHiHook(0x4E52F0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetSpellLevel);
		pearl->WriteHiHook(0x4E54B0,SPLICE_,EXTENDED_,THISCALL_,(void*)GetSpellCost);

		/*
+	signed int __thiscall Necromancy_GetCreature2Animate(void *this);
+	float __thiscall Necromancy_GetPower(int this, char a2);
+	int __thiscall GetDailyManaRestorePoints(int this);
+	int __thiscall GetScootingRadius(int this);
+	float __thiscall GetArcheryPower(int this);
+	float __thiscall GetOffencePower(int this);
+	float __thiscall GetArmorerPower(_Hero_ *this);
+	int __thiscall GetEstatesPower(int this);
+	float __thiscall GetEagleEyePower(int this);
+	float __thiscall GetDiplomacyPower(int this);
+	float __thiscall GetResistancePower(int this);
+	float __thiscall GetLearningPower(int this);
+	float __thiscall GetIntelligencePower(int this);
+	float __thiscall GetFirstAidPower(int this);
+	signed int __thiscall CalcHeroMovementPoints(int this, char a2);
+	signed int __thiscall GetSpellDurationBonus(void *this);
+	int __thiscall GetPowerOfHeroSpell(char *Hp, int a2, int MagicScool);
	GetSpellLevelPower4Hero - ����!
+	int __thiscall CalcSpellCost4Hero(char *Hp, int a2, void *MonArr, int MagicScool);
		
		
int __thiscall HeroSpellSpecialityEffect(_Hero_ *this, int spell, int heroLevel, signed int effect);
signed __int64 __thiscall Hero_004E59D0_SorceryEffect(_Hero_ *this, int spell, signed int damage, _CombatMonster_ *a4); // idb
		*/
	}


	/*switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}*/
	return TRUE;
}

