// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.
#include "stdafx.h"

Patcher * globalPatcher;
PatcherInstance *ice;

int __stdcall NewHasArtifact(HiHook* h, HERO* hero, int art)
{
	int ret = CALL_2(int, __thiscall, h->GetDefaultFunc(), hero, art);

	if (h->GetReturnAddress() < 0x700000 && h->GetReturnAddress() > 0x400000) //������� ������ SoD;
	{
		if (art == 90) return true;
	}

	return ret;
}

int __fastcall new_sub_004B14A0(HiHook* h, int mip, char a2, signed int a3, int a4, int a5, int a6, int a7, int a8, float a9)
{
   int ret = CALL_9(int, __fastcall, h->GetDefaultFunc(), mip, a2, a3, a4, a5, a6, a7, a8, a9);


	//int terrain_type = *(int*)(mip + 4) << 24 >> 24;

	//if (terrain_type == 8) ret = 9999;

	return ret;
}



BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	if (ul_reason_for_call == DLL_PROCESS_ATTACH)
	{
		globalPatcher = GetPatcher();
		ice =  globalPatcher->CreateInstance("Helcaraxe");
		ice->WriteHiHook(0x4D9460, SPLICE_, EXTENDED_, THISCALL_, (void*)NewHasArtifact);
		ice->WriteHiHook(0x4B14A0, SPLICE_, EXTENDED_, FASTCALL_, (void*)new_sub_004B14A0);
	}
	return TRUE;
}

