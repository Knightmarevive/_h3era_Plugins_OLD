
#include <vcl.h>
#pragma hdrstop

#include "EraButtons.h"

 /***/


#include <System.hpp>


const char BUTTONS_DLL_NAME[] = "buttons.dll";



typedef DynamicArray< StrLib.TArrayOfStr > TButtonsTable;
/*O*/


AssocArrays.TAssocArray ButtonNames;
int hButtons = 0;
PPOINTER ExtButtonsTable;
PINTEGER ExtNumButtons;
TButtonsTable ButtonsTable;
int ButtonID = 400;
int NumButtons = 0;


void __fastcall LoadButtons( )
{


/*O*/
  Files.TFileLocator Locator;
/*O*/
  Files.TFileItemInfo ItemInfo;
  String FileName;
  String FileContents;
  StrLib.TArrayOfStr Lines;
  StrLib.TArrayOfStr Line;
  int NumLines = 0;
  String ButtonName;
  int i = 0;
  int y = 0;
  Locator = Files.TFileLocator.Create;
  ItemInfo = NULL;
  // * * * * * //
  Locator.DirPath = BUTTONS_PATH;
  Locator.InitSearch( "*.btn" );
  while ( Locator.NotEnd )
  {
    FileName = SysUtils.AnsiLowerCase( Locator.GetNextItem( Files.TItemInfo( ItemInfo ) ) );
    if ( ! ItemInfo.IsDir & ( SysUtils.ExtractFileExt( FileName ) == ".btn" ) & ItemInfo.HasKnownSize & ( ItemInfo.FileSize > 0 ) )
    {
      /*!*/
      Assert( Files.ReadFileContents( String( BUTTONS_PATH ) + "\\" + FileName, FileContents ) );
      Lines = StrLib.Explode( SysUtils.Trim( FileContents ), "\x0d\x0a" );
      NumLines = Length( Lines );
      for ( int stop = NumLines - 1, i = 0; i <= stop; i++)
      {
        Line = StrLib.Explode( SysUtils.Trim( Lines[i] ), ';' );
        if ( Length( Line ) < NUM_BUTTON_COLUMNS )
        {
          DlgMes.Msg( "Invalid number of columns (" + SysUtils.IntToStr( Length( Line ) ) + ") on line " + SysUtils.IntToStr( i + 1 ) + " in file \"" + FileName + "\".\x0d\x0a" + "Expected " + SysUtils.IntToStr( NUM_BUTTON_COLUMNS ) + " columns" );
        } // .if
        else
        {
          Line[COL_TYPE] = SysUtils.AnsiLowerCase( Line[COL_TYPE] );
          for ( int stop = NUM_BUTTON_COLUMNS - 1, y = 0; y <= stop; y++)
          {
            if ( Line[y] == "" )
            {
              Line[y] = '\x00';
            } // .if
          } // .for
          if ( Line[COL_TYPE] == TYPENAME_ADVMAP )
          {
            Line[COL_TYPE] = TYPE_ADVMAP;
          } // .if
          else
            if ( Line[COL_TYPE] == TYPENAME_TOWN )
            {
              Line[COL_TYPE] = TYPE_TOWN;
            } // .ELSEIF
            else
              if ( Line[COL_TYPE] == TYPENAME_HERO )
              {
                Line[COL_TYPE] = TYPE_HERO;
              } // .ELSEIF
              else
                if ( Line[COL_TYPE] == TYPENAME_HEROES )
                {
                  Line[COL_TYPE] = TYPE_HEROES;
                } // .ELSEIF
                else
                  if ( Line[COL_TYPE] == TYPENAME_BATTLE )
                  {
                    Line[COL_TYPE] = TYPE_BATTLE;
                  } // .ELSEIF
                  else
                    if ( Line[COL_TYPE] == TYPENAME_DUMMY )
                    {
                      Line[COL_TYPE] = TYPE_DUMMY;
                    } // .ELSEIF
                    else
                    {
            /*!*/
                      Assert( false );
                    } // .else
          ButtonName = Line[COL_NAME];
          if ( ButtonNames[ButtonName] != NULL )
          {
            DlgMes.Msg( String( "Duplicate button name (\"" ) + ButtonName + "\") on line " + SysUtils.IntToStr( i + 1 ) + " in file \"" + FileName + "\"" );
          } // .if
          else
          {
            ButtonNames[ButtonName] = Ptr( ButtonID );
            Line[COL_NAME] = SysUtils.IntToStr( ButtonID );
            ButtonID++;
            ButtonsTable.Length = NumButtons + 1;
            ButtonsTable[NumButtons] = Line;
            NumButtons++;
          } // .else
        } // .else
      } // .for
    } // .if
    SysUtils.FreeAndNil( ItemInfo );
  } // .while
  Locator.FinitSearch;
  ExtButtonsTable = (PPOINTER) ((void*) ButtonsTable );
  ExtNumButtons = NumButtons;
  // * * * * * //
  SysUtils.FreeAndNil( Locator );
} // .procedure LoadButtons 



int __stdcall GetButtonID( const String ButtonName )
{
  int result = 0;
  result = ((int) ButtonNames[ButtonName] );
  if ( result == 0 )
  {
    result = - 1;
  } // .if
  return result;
} // .function GetButtonID



void __stdcall OnAfterWoG( PEvent Event )
{
  /* Connect to Buttons.dll */
  hButtons = Windows.LoadLibrary( BUTTONS_DLL_NAME );
  /*!*/
  Assert( hButtons != 0 );
  ExtButtonsTable = GetProcAddress( hButtons, "ButtonsTable" );
  ExtNumButtons = GetProcAddress( hButtons, "NumButtons" );
  /*!*/
  Assert( ExtButtonsTable != NULL );
  /*!*/
  Assert( ExtNumButtons != NULL );
  LoadButtons();
} // .procedure OnAfterWoG

void EraButtons_initialization()
{
  NumButtons = 0;
  ButtonNames = AssocArrays.NewSimpleAssocArr( Crypto.AnsiCRC32, SysUtils.AnsiLowerCase );
  GameExt.RegisterHandler( OnAfterWoG(), "OnAfterWoG" );
}

class EraButtons_unit
{
public:
EraButtons_unit()
{
  EraButtons_initialization();
}
};
EraButtons_unit _EraButtons_unit;
