#ifndef EraButtonsH
#define EraButtonsH
/*
DESCRIPTION:  Adds custom buttons support using modified Buttons plugin by MoP 
AUTHOR:       Alexander Shostak (aka Berserker aka EtherniDee aka BerSoft)
*/

/***/


#include <System.hpp>  /***/

#include <windows.hpp>
#include <sysutils.hpp>
#include "Crypto.h"
#include "StrLib.h"
#include "Files.h"
#include "AssocArrays.h"
#include "DlgMes.h"
#include "Core.h"
#include "GameExt.h"

const char BUTTONS_PATH[] = "Data\\Buttons";
const int NUM_BUTTON_COLUMNS = 10;
  
  /* Columns */
const int COL_TYPE = 0;
const int COL_NAME = 1;
const int COL_DEF = 2;
const int COL_X = 3;
const int COL_Y = 4;
const int COL_WIDTH = 5;
const int COL_HEIGHT = 6;
const int COL_LONGHINT = 7;
const int COL_SHORTHINT = 8;
const int COL_HOTKEY = 9;
  
  /* Button screen */
const char TYPENAME_ADVMAP[] = "advmap";
const char TYPENAME_TOWN[] = "town";
const char TYPENAME_HERO[] = "hero";
const char TYPENAME_HEROES[] = "heroes";
const char TYPENAME_BATTLE[] = "battle";
const char TYPENAME_DUMMY[] = "dummy";  // Button is not shown

const char TYPE_ADVMAP = '0';
const char TYPE_TOWN = '1';
const char TYPE_HERO = '2';
const char TYPE_HEROES = '3';
const char TYPE_BATTLE = '4';
const char TYPE_DUMMY = '9';
int __stdcall GetButtonID( const String ButtonName );
  
  
/***/

#endif //  EraButtonsH