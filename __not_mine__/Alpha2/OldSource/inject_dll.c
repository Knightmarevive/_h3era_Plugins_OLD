#define UNICODE
#include <windows.h>
#include <stdio.h>

//-------------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------------
typedef wchar_t wchar;

//-------------------------------------------------------------------------------------------------------
//
//-------------------------------------------------------------------------------------------------------
int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, char *pCmdLine, int nShowCmd )
{
    int argc;
    wchar **argv = CommandLineToArgvW( GetCommandLineW(), &argc );

    if( argc < 2 )
    {
        MessageBox( NULL, L"usage: inject_dll.exe path/name.exe path/name.dll path2/name2.dll ...",
                        L"inject_dll.exe", MB_ICONERROR );
        return 0;
    }

    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    ZeroMemory( &pi, sizeof(pi) );
    si.cb = sizeof(si);

    if( !CreateProcess( NULL, argv[1], NULL, NULL, FALSE, CREATE_SUSPENDED, NULL, NULL, &si, &pi ) )
    {
        MessageBox( NULL, L"CreateProcess failed", L"inject_dll.exe", MB_ICONERROR );
        return 0;
    }

    LPTHREAD_START_ROUTINE pfnLoadLiraryW =
        (LPTHREAD_START_ROUTINE)GetProcAddress( GetModuleHandle(L"kernel32.dll"), "LoadLibraryW" );

    int DLLCount = argc - 2;
    wchar **pDLLPaths = argv + 2;
    int i;
    for( i = 0; i < DLLCount; i++ )
    {
        int DLLPathSize = ( wcslen(pDLLPaths[i]) + 1 )*2;

        void *pMem = VirtualAllocEx( pi.hProcess, NULL, DLLPathSize, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE );
        if( !pMem )
        {
            MessageBox( NULL, L"VirtualAllocEx failed", L"inject_dll.exe", MB_ICONERROR );
            return 0;
        }

        if( !WriteProcessMemory( pi.hProcess, pMem, pDLLPaths[i], DLLPathSize, NULL ) )
        {
            MessageBox( NULL, L"WriteProcessMemory failed", L"inject_dll.exe", MB_ICONERROR );
            return 0;
        }

        HANDLE hThread = CreateRemoteThread( pi.hProcess, NULL, 0, pfnLoadLiraryW, pMem, 0, NULL );
        if( !hThread )
        {
            MessageBox( NULL, L"CreateRemoteThread failed", L"inject_dll.exe", MB_ICONERROR );
            return 0;
        }

        WaitForSingleObject( hThread, INFINITE );
        VirtualFreeEx( pi.hProcess, pMem, 0, MEM_RELEASE );

        DWORD Result;
        GetExitCodeThread( hThread, &Result );
        if( Result == 0 )
        {
            wchar Msg[wcslen(pDLLPaths[i]) + 20];
            swprintf( Msg, L"%s wasn't injected", pDLLPaths[i] );
            MessageBox( NULL, Msg, L"inject_dll.exe", MB_ICONERROR );
        }
    }
    ResumeThread( pi.hThread );
    return 0;
}
