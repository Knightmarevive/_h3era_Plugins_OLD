
#include "stdafx.h"

extern int all_creatures;

DWORD CreatureSpellPowerMultiplier[MONSTERS_AMOUNT];
DWORD CreatureSpellPowerDivider[MONSTERS_AMOUNT];
DWORD CreatureSpellPowerAdder[MONSTERS_AMOUNT];

DWORD Necromancy_without_artifacts[4];
DWORD Necromancy_with_artifacts[4];

char hasSantaGuards[MONSTERS_AMOUNT];
//DWORD SantaGuardsType[MONSTERS_AMOUNT];
//DWORD UpgradedSantaGuardsType[MONSTERS_AMOUNT];

char isGhost[MONSTERS_AMOUNT];
char isRogue[MONSTERS_AMOUNT];

char isEnchanter[MONSTERS_AMOUNT];
char isSorceress[MONSTERS_AMOUNT];

char isHellSteed[MONSTERS_AMOUNT];
char isHellSteed2[MONSTERS_AMOUNT];
char isHellSteed3[MONSTERS_AMOUNT];

typedef enum 
{ACAST_BIND,
ACAST_BLIND,
ACAST_DISEASE,
ACAST_CURSE,
ACAST_AGE, 
ACAST_STONE,
ACAST_PARALIZE, 
ACAST_POIZON, 
ACAST_ACID, 
ACAST_DEFAULT}
AFTERCAST_ABILITY;

char aftercast_abilities_table[MONSTERS_AMOUNT];

//=============================================
typedef enum
{ATT_VAMPIRE,
ATT_THUNDER,
ATT_DEATHSTARE,
ATT_DISPEL,
ATT_DISRUPT,
ATT_DEFAULT}
ATTACK_ABILITY;

char attack_abilities_table[MONSTERS_AMOUNT];
//=============================================
typedef enum
{RESIST_DWARF20,
 RESIST_DWARF40,
 RESIST_123LVL,
 RESIST_1234LVL,
 RESIST_MAGICIMMUNE,
 RESIST_ASAIR,
 RESIST_ASEARTH,
 RESIST_ASFIRE,
 RESIST_DEFAULT}
MAGIC_RESISTANCE;

char magic_resistance_table[MONSTERS_AMOUNT];
//=============================================

typedef enum
{
	VULN_HALF,
	VULN_QUATER,
	VULN_LIGHTING,
	VULN_SHOWER,
	VULN_ICE,
	VULN_FIRE,
	VULN_GOLD,
	VULN_DIAMOND,
	VULN_DEFAULT
} 
MAGIC_VULNERABILITY;

char magic_vulnerability_table[MONSTERS_AMOUNT];
//=============================================
char missiles_table[MONSTERS_AMOUNT];
//=============================================

char spell_1_table[MONSTERS_AMOUNT]; int  spell_1_table_ptr = (int)spell_1_table;
char spell_2_table[MONSTERS_AMOUNT]; int  spell_2_table_ptr = (int)spell_2_table;
char spell_3_table[MONSTERS_AMOUNT]; int  spell_3_table_ptr = (int)spell_3_table;
//=============================================

int skeltrans[MONSTERS_AMOUNT];
//=============================================

int upgtable[MONSTERS_AMOUNT];

char special_missiles_table[MONSTERS_AMOUNT];
float fire_shield_table[MONSTERS_AMOUNT];
float respawn_table[MONSTERS_AMOUNT];
/*char mana_decrease[MONSTERS_AMOUNT];
char mana_increase[MONSTERS_AMOUNT];

char no_wall_table[MONSTERS_AMOUNT];
char no_range_table[MONSTERS_AMOUNT];


char mana_channel_table[MONSTERS_AMOUNT];
char mana_leak_table[MONSTERS_AMOUNT];

char enchanted[MONSTERS_AMOUNT];


char resource_type_table[MONSTERS_AMOUNT];
char resource_amount_table[MONSTERS_AMOUNT];

char block_table[MONSTERS_AMOUNT];

char double_strike[MONSTERS_AMOUNT];*/


//=============================================




//===================================

extern MONSTER_PROP new_monsters[MONSTERS_AMOUNT];

extern "C" __declspec(dllexport) void ChangeCreatureTable(int target, char* buf) {
	char *c;


	c = strstr(buf, "CreatureSpellPowerMultiplier=");
	if (c != NULL) CreatureSpellPowerMultiplier[target] = atoi(c + strlen("CreatureSpellPowerMultiplier="));

	c = strstr(buf, "CreatureSpellPowerDivider=");
	if (c != NULL) CreatureSpellPowerDivider[target] = atoi(c + strlen("CreatureSpellPowerDivider="));

	c = strstr(buf, "CreatureSpellPowerAdder=");
	if (c != NULL) CreatureSpellPowerAdder[target] = atoi(c + strlen("CreatureSpellPowerAdder="));

	c = strstr(buf, "hasSantaGuards=");
	if (c != NULL) hasSantaGuards[target] = atoi(c + strlen("hasSantaGuards="));


	c = strstr(buf, "isRogue=");
	if (c != NULL) isRogue[target] = atoi(c + strlen("isRogue="));

	c = strstr(buf, "isGhost=");
	if (c != NULL) isGhost[target] = atoi(c + strlen("isGhost="));


	c = strstr(buf, "isEnchanter=");
	if (c != NULL) isEnchanter[target] = atoi(c + strlen("isEnchanter="));

	c = strstr(buf, "isSorceress=");
	if (c != NULL) isSorceress[target] = atoi(c + strlen("isSorceress="));


	c = strstr(buf, "isHellSteed=");
	if (c != NULL) isHellSteed[target] = atoi(c + strlen("isHellSteed="));
	c = strstr(buf, "isHellSteed2=");
	if (c != NULL) isHellSteed2[target] = atoi(c + strlen("isHellSteed2="));
	c = strstr(buf, "isHellSteed3=");
	if (c != NULL) isHellSteed3[target] = atoi(c + strlen("isHellSteed3="));

	//end of majaczek
	c = strstr(buf, "Level=");
	if (c != NULL)
		new_monsters[target].level = atoi(c + strlen("Level="));

	c = strstr(buf, "Flags=");
	if (c != NULL)
		new_monsters[target].flags = atoi(c + strlen("Flags="));


	c = strstr(buf, "Town=");
	if (c != NULL)
		new_monsters[target].town = atoi(c + strlen("Town="));
	//			
	///*
	//Singular	Plural	Wood	Mercury	Ore	Sulfur	Crystal	Gems	Gold	Fight Value	AI Value	Growth	Horde Growth	
	//Hit Points	Speed	Attack	Defense	Low	High	
	//Shots	Spells	Low	High	Ability Text	Attributes */
	//
				c = strstr(buf,"Adv.high=" );
				if (c!=NULL) new_monsters[target].adv_high=
							atoi(c+strlen("Adv.high=" ));
	//			else new_monsters[target].adv_high = 0;
	//
				c = strstr(buf,"Adv.low=" );
				if (c!=NULL) new_monsters[target].adv_low=
							atoi(c+strlen("Adv.low=" ));
	//			else new_monsters[target].i_AdvLow = 0;
	//
				c = strstr(buf,"AI_value=" );
				if (c!=NULL) new_monsters[target].ai_value=
							atoi(c+strlen("AI_value=" ));
	//
				c = strstr(buf,"Attack=" );
				if (c!=NULL) new_monsters[target].attack =
							atoi(c+strlen("Attack=" ));
	//
				c = strstr(buf,"CostCrystal=" );
				if (c!=NULL) new_monsters[target].cost_crystal=
							atoi(c+strlen("CostCrystal=" ));
	//
				c = strstr(buf,"CostGems=" );
				if (c!=NULL) new_monsters[target].cost_gems =
							atoi(c+strlen("CostGems=" ));
	//
				c = strstr(buf,"CostGold=" );
				if (c!=NULL) new_monsters[target].cost_gold=
							atoi(c+strlen("CostGold=" ));
	//
				c = strstr(buf,"CostMercury=" );
				if (c!=NULL) new_monsters[target].cost_mercury=
							atoi(c+strlen("CostMercury=" ));
	//
				c = strstr(buf,"CostOre=" );
				if (c!=NULL) new_monsters[target].cost_ore=
							atoi(c+strlen("CostOre=" ));
	//
				c = strstr(buf,"CostSulfur=" );
				if (c!=NULL) new_monsters[target].cost_sulfur=
							atoi(c+strlen("CostSulfur=" ));
	//
				c = strstr(buf,"CostWood=" );
				if (c!=NULL) new_monsters[target].cost_wood=
							atoi(c+strlen("CostWood=" ));
	//
				c = strstr(buf,"DamageHigh=" );
				if (c!=NULL) new_monsters[target].damage_high =
							atoi(c+strlen("DamageHigh=" ));
	//
				c = strstr(buf,"DamageLow=" );
				if (c!=NULL) new_monsters[target].damage_low=
							atoi(c+strlen("DamageLow=" ));
	//
				c = strstr(buf,"Defence=" );
				if (c!=NULL) new_monsters[target].defence=
							atoi(c+strlen("Defence=" ));
	//
				c = strstr(buf,"FightValue=" );
				if (c!=NULL) new_monsters[target].fight_value=
							atoi(c+strlen("FightValue=" ));
	//
				c = strstr(buf,"Growth=" );
				if (c!=NULL) new_monsters[target].growth=
							atoi(c+strlen("Growth=" ));
	//
				c = strstr(buf,"HP=" );
				if (c!=NULL) new_monsters[target].hp=
							atoi(c+strlen("HP=" ));
	//
				c = strstr(buf,"Horde=" );
				if (c!=NULL) new_monsters[target].horde_growth=
							atoi(c+strlen("Horde=" ));
	//
				c = strstr(buf,"Shots=" );
				if (c!=NULL) new_monsters[target].shots=
							atoi(c+strlen("Shots=" ));
	//
				c = strstr(buf,"Speed=" );
				if (c!=NULL) new_monsters[target].speed=
							atoi(c+strlen("Speed=" ));
	//
				c = strstr(buf,"Spells=" );
				if (c!=NULL) new_monsters[target].spells=
							atoi(c+strlen("Spells=" ));
	//
	///*Time between fidgets	Walk Animation Time	Attack Animation Time	
	//Flight Animation Distance	Upper-right Missile Offset		
	//Right Missile Offset		Lower-right Missile Offset		Missile Frame Angles												
	//Troop Count Location Offset	Attack Climax Frame*/
	//
	///*
	//			c = strstr(buf,"TBF=" );
	//			if (c!=NULL) new_monsters[target].i_Spells=
	//						atoi(c+strlen("TBF=" ));
	//*/


	c = strstr(buf, "Spell effect=");
	if (c != NULL) aftercast_abilities_table[target] =
		atoi(c + strlen("Spell effect="));

	c = strstr(buf, "Attack effect=");
	if (c != NULL) attack_abilities_table[target] =
		atoi(c + strlen("Attack effect="));

	c = strstr(buf, "Resistance effect=");
	if (c != NULL) magic_resistance_table[target] =
		atoi(c + strlen("Resistance effect="));

	c = strstr(buf, "Vulnerability effect="); // majaczek
	if (c != NULL) magic_vulnerability_table[target] =
		atoi(c + strlen("Vulnerability effect="));




	c = strstr(buf, "Spell 1=");
	if (c != NULL) spell_1_table[target] =
		atoi(c + strlen("Spell 1="));


	c = strstr(buf, "Spell 2=");
	if (c != NULL) spell_2_table[target] =
		atoi(c + strlen("Spell 2="));


	c = strstr(buf, "Spell 3=");
	if (c != NULL) spell_3_table[target] =
		atoi(c + strlen("Spell 3="));

	c = strstr(buf, "Sktransformer=");
	if (c != NULL) skeltrans[target] =
		atoi(c + strlen("Sktransformer="));

	c = strstr(buf, "Shot type=");
	if (c != NULL) special_missiles_table[target] =
		atoi(c + strlen("Shot type="));

	c = strstr(buf, "Fire shield=");
	if (c != NULL) fire_shield_table[target] =
		(float)atof(c + strlen("Fire shield="));

	c = strstr(buf, "Self-resurrection=");
	if (c != NULL) respawn_table[target] =
		(float)atof(c + strlen("Self-resurrection="));

}
void LoadCreatureConfig(int target)
{
		char *buf, *c, fname[256];
        FILE *fdesc;
		
		/*
		sprintf(fname,"Mods\\Amethyst Upgrades\\Data\\creatures\\%u.cfg",target);
        if(!FileExists(fname)) */
	        sprintf(fname,"Mods\\Knightmare Kingdoms\\Data\\creatures\\%u.cfg",target);
		if(!FileExists(fname)) 
			sprintf(fname,"Data\\creatures\\%u.cfg",target);
        if(fdesc=fopen(fname,"r"))
        {
            //----------
            fseek (fdesc , 0 , SEEK_END);
            int fdesc_size=ftell(fdesc);
            rewind(fdesc);
            //----------
            buf=(char*)malloc(fdesc_size+1);
            fread(buf,1,fdesc_size,fdesc);
            buf[fdesc_size]=0;
            fclose(fdesc);
            //begin of majaczek

			ChangeCreatureTable(target, buf);


            free(buf);
        }
}

void FillTables()
{
   WIN32_FIND_DATAA ffd;
   HANDLE hFind = INVALID_HANDLE_VALUE;

   /*
       hFind = FindFirstFile(_T("Mods\\Amethyst Upgrades\\Data\\creatures\\*.cfg"), &ffd);  
   if (INVALID_HANDLE_VALUE == hFind) 
       hFind = FindFirstFile(_T("Mods\\Knightmare Kingdoms\\Data\\creatures\\*.cfg"), &ffd);
   if (INVALID_HANDLE_VALUE == hFind)
       hFind = FindFirstFile(_T("Data\\creatures\\*.cfg"), &ffd);
	*/
   hFind = FindFirstFileA("Data\\creatures\\*.cfg", &ffd);

   if (INVALID_HANDLE_VALUE == hFind)  {return;} 

   do
   {
      if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
		 int creature = atoi(ffd.cFileName);
		 if(creature!=0)
		 {
			LoadCreatureConfig(creature);
		 }
      }
   }
   while (FindNextFileA(hFind, &ffd) != 0);
   FindClose(hFind);
}
//=============================================
//fear




//=============================================
//special shots

int __fastcall GetShotType(int creature, int* slot)
{
   return special_missiles_table[creature];
}

__declspec(naked) void f43F72C_Hook()
{
__asm
{
	mov ecx, eax
	mov edx, esi
	call GetShotType
	cmp eax, 1
	je j43F735
	cmp eax, 2
	je j43FB27

	mov eax, 0x43FA31
	jmp eax
	
	j43FB27:
	mov eax, 0x43FB27
	jmp eax

	j43F735:
	mov eax, 0x0043F735
	jmp eax
}
}

__declspec(naked) void f41ED5A_Hook()
{
__asm
{
	push ecx
	push edx
	mov ecx, eax
	mov edx, -1
	call GetShotType
	cmp eax, 0
	jne j41ED69
	mov eax, 0x41ED6D
	pop edx
	pop ecx
	jmp eax
	j41ED69:
	mov eax, 0x41ED69
	pop edx
	pop ecx
	jmp eax

}
}

//============================================
//majaczek


__declspec(naked) void IsAngel3(void)
{
	__asm {
		mov    eax, [esi + 0x34]
		//cmp    eax, 0x0D
		//je     l_Ok
		//cmp    eax, 150
		//je     l_Ok

		push ebx
		cmp eax, 0
		jl l_bad
		cmp eax, MONSTERS_AMOUNT
		jge l_bad

		mov ebx, spell_1_table_ptr
		add ebx, eax
		mov ebx, DS : [EBX]
		and ebx, 0xff
		cmp ebx, 0
		jne l_bad
		mov ebx, spell_2_table_ptr
		add ebx, eax
		mov ebx, DS : [EBX]
		and ebx, 0xff
		cmp ebx, 0
		jne l_bad
		mov ebx, spell_3_table_ptr
		add ebx, eax
		mov ebx, DS : [EBX]
		and ebx, 0xff
		cmp ebx, 0
		jne l_bad
		jmp l_Ok
	}
l_bad:
	__asm {
		pop ebx

		mov    esi, [esi + 0x4C]
		ret
	}
l_Ok:
	__asm {
		mov ebx, eax
		shl ebx, 2

		//mov    eax, 0x0D
		mov    esi, [esi + 0x4C]
		mov    eax, esi
		
		push edx
		mul    CreatureSpellPowerMultiplier[ebx]
		div    CreatureSpellPowerDivider[ebx]
		add    eax, CreatureSpellPowerAdder[ebx]
		pop edx

		mov esi, eax
		pop ebx
		mov    eax, 0x0D
		ret
	}
}

/*
__declspec(naked) void f_num_Hook()
{
__asm
{
	
}
}
*/

//Creature SpellPower
DWORD f_75D2BB_temp;
__declspec(naked) void f_75D2BB_Hook()
{
	__asm
	{
		pop edi
		pop esi
		pop ebx
		pop ebp
		pusha

		mov ebx, DS:[esi + 0x34]
		
		mov ecx, CreatureSpellPowerMultiplier[ebx]
		mul ecx

		mov ecx, CreatureSpellPowerDivider[ebx]
		mov edx, 0
		div ecx

		mov ecx, CreatureSpellPowerAdder[ebx]
		add eax, ecx

		mov f_75D2BB_temp, EAX

		popa
		mov EAX, f_75D2BB_temp
		retn

	}
}


//Necromancy
__declspec(naked) void f_4e3ed0_Hook()
{
	__asm
	{

	PUSH ESI
	MOV ESI, ECX
	XOR EAX, EAX
	LEA ECX, [ESI + 0x12D]
		
	pusha
		
	mov al, [esi + 0xd5]
	
	//mov edx Necromancy_without_artifacts[eax]
	lea ebx, [Necromancy_without_artifacts + eax*4]

	mov edx, [ebx]
	mov DS:[0x4E3F3A], edx
	
	
	XOR EAX, EAX

	mov edx, Necromancy_with_artifacts[1]
	mov DS : [0x004e3f33], edx

	mov edx, Necromancy_with_artifacts[2]
	mov DS : [0x004e3f2a], edx

	mov edx, Necromancy_with_artifacts[3]
	mov DS : [0x004e3f1f], edx

	popa

	_unknown_1:
	CMP  DS : [ECX], 0x82
	je _unknown_3
	inc eax
	add ecx, 8
	CMP EAX, 0x13
	jl _unknown_1
	MOV EAX,  DS : [0x660B68]
	MOV EAX,  DS : [EAX + 0x1058]
	cmp eax, -1
	je _unknown_2
	MOV EDX,  DS : [0x660B6C]
	LEA ECX, [EAX * 2 + EAX]
	MOV EAX,  DS : [ECX * 8 + EDX]
	MOV ECX, ESI
	push eax
	CALL CS: 0x004D9460
	mov eax, 0x4e3f39;
	jmp eax

	_unknown_2:
	mov eax, 0x4e3f39
	jmp eax
	
	_unknown_3:

	mov eax, 0x4e3f14
	jmp eax

	//// @ 0x4e3f14
	//// this part don't work in my dissassembler
	//// dissassembled by hand from old 8086 instructions
	//mov al, 0xd5
	//add al, [ebx+esi] // or reverse ???
	//cmp al, 0x03

	}
}

//Hell Steed
__declspec(naked) void f_75CA39_Hook()
{
__asm
{
	pusha
	mov  eax , SS:[EBP-0x2c]
	mov  bh , isHellSteed[eax]
	cmp  bh , 0
	je _isNotHellSteed
	
	_isHellSteed:
	popa
	MOV EAX, SS : [EBP - 0x20]
	CMP DWORD PTR DS : [EAX], 6
	JNE _another_action
	mov eax, 0x0075ca4a
	jmp eax

	_another_action:
	mov eax, 0x0075CA76
	jmp eax
	//mov eax, 0x0075ca42
	//jmp eax
	
	_isNotHellSteed:
	popa
	mov eax, 0x0075ca76
	jmp eax
}
}
//
__declspec(naked) void f_75E8BA_Hook()
{
__asm
{
	
	pusha
	mov  eax , SS:[EBP-0x10]
	mov  bh , isHellSteed2[eax]
	cmp  bh , 0
	je _isNotHellSteed
	
	_isHellSteed:
	popa	
	mov eax, 0x0075e8c3
	jmp eax
	
	_isNotHellSteed:
	popa
	mov eax, 0x0075e8da
	jmp eax
}
}
//
__declspec(naked) void f_760723_Hook()
{
__asm
{
	
	pusha
	mov  eax , SS:[EBP-0x0c]
	mov  bh , isHellSteed3[eax]
	cmp  bh , 0
	je _isNotHellSteed
	
	_isHellSteed:
	popa	
		mov eax, 0x0076072c
		jmp eax
	_isNotHellSteed:
	popa
		mov eax, 0x00760743
		jmp eax
}
}


//Sorceress
__declspec(naked) void f_75C96C_Hook()
{
__asm
{
	pusha
	mov ecx, SS:[EBP-0x2c]
	mov dh, isSorceress[ecx]
	cmp dh, 0
	je _isNotSorceress
	
	_isSorceress:
	popa
		mov eax, 0x0075c979
		jmp eax
	_isNotSorceress:
	popa
		mov eax, 0x0075ca39
		jmp eax
}
}


//Enchanter bad
__declspec(naked) void f_447ED9_Hook()
{
__asm
{   

	pusha
	mov EBX, DS:[EAX+0x34]
	mov ch, isEnchanter[EBX]
	cmp ch, 0
	je _isNotEnchanter
	
	_isEnchanter:
	popa
	//mov DS:[eax+0x34], 0x88
	mov eax, 0x00447ee2
	jmp eax
	//push 0x00447ee2
	//retf

	_isNotEnchanter:
	popa
	mov eax, 0x00447f1b
	jmp eax
	//push  0x00447f1b
	//retf
	

}
}

//Enchanter
__declspec(naked) void f_4650dd_Hook()
{
	__asm
	{
		pusha
		//mov eax, DS:[EBX+0x34]
		add eax, 0x88
		mov ch, isEnchanter[EAX]
		cmp ch, 0
		je _isNotEnchanter

		_isEnchanter:
		popa
		MOV ECX, DS : [ESI + 0x132C0]
		mov eax, 0x004650e9
		jmp eax
	
		_isNotEnchanter:
		popa
		mov eax, 0x004653e2
		jmp eax
	}
}

//Santa Gremlin
__declspec(naked) void f_75D0F2_Hook()
{
__asm
{
	pusha
	mov dh, hasSantaGuards[ecx]
	cmp dh, 0
	je  _hasntSantaGuards
	_hasSantaGuards:
	popa
	mov eax, 0x0075d0fb
	jmp eax

	_hasntSantaGuards:
	popa	
	mov eax, 0x0075d112
	jmp eax
}
}

 
//Ghost
__declspec(naked) void f_756B1F_Hook()
{

__asm
{   
	pusha
	mov EBX, DS:[EAX+0x34]

	mov CH, isGhost[EBX]
	cmp ch, 0
	je _isNotGhost
	
	_isGhost:
	mov DS:[0x756C9A], EBX
	popa
	mov eax, 0x00756b2c
	jmp eax
	_isNotGhost:
	popa
	mov eax, 0x00756cad
	jmp eax
}
}


__declspec(naked) void f_MoP_Rogue_Hook()
{
__asm{
	//	; Change function :

	mop_007572FA:
		PUSH ESI
		MOV ESI,ECX
		CMP DWORD PTR DS : [ESI + 0x129],3 // - Scouting level
		PUSH EDX
		PUSH EBX
		JGE SHORT mop_00757332
		LEA ECX,DWORD PTR DS : [ESI + 0x91] // - creatures slots
		MOV EDX,7
	mop_00757313 :
		MOV EBX,DWORD PTR DS : [ECX]
		CMP EBX,-1
		JE SHORT mop_0075732C
		CMP BYTE PTR DS : [EBX + isRogue],0 // - check ability
		MOV EAX,3
		JE SHORT mop_0075732C
	mop_00757328 :
		POP EBX
		POP EDX
		POP ESI
		RETN
	mop_0075732C :
		ADD ECX,4
		DEC EDX
		JNZ SHORT mop_00757313
	mop_00757332 :
		MOV EAX,DWORD PTR DS : [ESI + 0x129]
		JMP SHORT mop_00757328

	}
}
/*
DWORD temp_rogue;
__declspec(naked) void f_004E6050_Hook()
{
__asm
{
label_004E6050:
PUSH ESI
MOV ESI, ECX
CMP DWORD PTR DS : [ESI + 0x129], 3
JGE SHORT label_004E6075

//PUSH 0x8F
jmp label_majaczek1
label_majaczek3:

LEA ECX, [ESI + 0x91]
mov eax, 0x0044AA90
Call eax
TEST EAX, EAX
MOV EAX, 3
JNZ SHORT label_004E607B
label_004E6075:
MOV EAX, DWORD PTR DS : [ESI + 0x129]
label_004E607B:
POP ESI
RETN

label_majaczek1:
pusha
label_majaczek2:

mov temp_rogue, 0x08f

popa
push temp_rogue;
jmp label_majaczek3
}
}
*/

//============================================
int __stdcall GetFireShield(int creature, int* slot)
{
	char t[256];

sprintf(t,"%i %x %f", creature, slot, respawn_table[creature]);


	//MessageBoxA(0,(LPCSTR)t,(LPCSTR)t,0);
	if (fire_shield_table[creature]>0.01)
	{
		*(int*)0x442E69 = creature*4 + (int)fire_shield_table;
		return 1;
	}
	else
	{
		return 0;
	}
}

__declspec(naked) void f442E61_Hook()
{
__asm
{
	push ecx
	push [ecx+0x34]
	call GetFireShield
	cmp eax, 0
	je j442e6e
	mov eax, 0x442e67
	jmp eax

	j442e6e:
	mov eax, 0x442e6e
	jmp eax

}
}


__declspec(naked) void f4225D6_Hook()
{
__asm
{
	push esi
	push [esi+0x34]
	call GetFireShield
	cmp eax, 0
	je j4225dc
	mov eax, 0x4225e6
	jmp eax
j4225dc:
	mov eax, 0x4225dc
	jmp eax

}
}

//============================================

void InitNecromancy() {
	/*
	std::ifstream str_necro;
	
	if (FileExists("Mods\\Amethyst Upgrades\\Data\\necromancy.cfg"))
		str_necro.open("Mods\\Amethyst Upgrades\\Data\\necromancy.cfg", std::ios_base::in);
	else if (FileExists("Mods\\Knightmare Kingdoms\\Data\\necromancy.cfg"))
		str_necro.open("Mods\\Knightmare Kingdoms\\Data\\necromancy.cfg", std::ios_base::in);
	else str_necro.open("Data\\necromancy.cfg", std::ios_base::in);
	*/
	bool good = true;
	FILE* str_necro;
	char *filename;

	/*if (FileExists(filename = "Data\\necromancy.cfg")) str_necro = fopen(filename, "r");
	else*/ /*if(FileExists(filename = "Mods\\Amethyst Upgrades\\Data\\necromancy.cfg")) str_necro = fopen(filename, "r");
	else*/ if (FileExists(filename = "Mods\\Knightmare Kingdoms\\Data\\necromancy.cfg")) str_necro = fopen(filename, "r");

	else if (FileExists(filename = "Data\\necromancy.cfg")) str_necro = fopen(filename, "r");
	else { good = false; }
	//str_necro=fopen(filename, "r");
	/*
	str_necro
		>> (Necromancy_without_artifacts[1])
		>> (Necromancy_without_artifacts[2])
		>> (Necromancy_without_artifacts[3])

		>> (Necromancy_with_artifacts[1])
		>> (Necromancy_with_artifacts[2])
		>> (Necromancy_with_artifacts[3]);
	*/
	if (good)
	fscanf(str_necro,"%ld%ld%ld%ld%ld%ld",
		(Necromancy_without_artifacts+1),
		(Necromancy_without_artifacts+2),
		(Necromancy_without_artifacts+3),

		(Necromancy_with_artifacts+1),
		(Necromancy_with_artifacts+2),
		(Necromancy_with_artifacts+3));
	
	if ((!good)
		|| all_creatures < Necromancy_without_artifacts[1] || all_creatures < Necromancy_without_artifacts[2] || all_creatures < Necromancy_without_artifacts[3]
		|| all_creatures < Necromancy_with_artifacts[1]    || all_creatures < Necromancy_with_artifacts[2]    || all_creatures < Necromancy_with_artifacts[3] ) {

		MessageBoxA(0, "necromancy.cfg", "Wrong config: ", 0);
		char log_[1024]; sprintf(log_, "%ld %ld %ld %ld %ld %ld",
			(Necromancy_without_artifacts[1]),
			(Necromancy_without_artifacts[2]),
			(Necromancy_without_artifacts[3]),

			(Necromancy_with_artifacts[1]),
			(Necromancy_with_artifacts[2]),
			(Necromancy_with_artifacts[3]));

		MessageBoxA(0,log_, "necromancy.cfg", 0);

		Necromancy_without_artifacts[1] = 56;
		Necromancy_without_artifacts[2] = 56;
		Necromancy_without_artifacts[3] = 56;

		Necromancy_with_artifacts[1] = 58;
		Necromancy_with_artifacts[2] = 60;
		Necromancy_with_artifacts[3] = 64;

		good = false;
	}

	///str_necro.close();
	if(good)fclose(str_necro);

	//majaczek necromancy
	//WriteHook((void*)0x4e3f14, (void*)f_4e3f14_Hook, HOOKTYPE_JUMP);
	WriteHook((void*)0x4e3ed0, (void*)f_4e3ed0_Hook, HOOKTYPE_JUMP);
	
}

void CreateAdditionalTables()
{
	//majaczek tables
	memset(hasSantaGuards,false,MONSTERS_AMOUNT); hasSantaGuards[173]=true;
	memset(isGhost,false,MONSTERS_AMOUNT); isGhost[159]=true;

	memset(isEnchanter,false,MONSTERS_AMOUNT); isEnchanter[136]=true;
	memset(isSorceress,false,MONSTERS_AMOUNT); isSorceress[193]=true;

	memset(isHellSteed,false,MONSTERS_AMOUNT);  isHellSteed[195]=true;
	memset(isHellSteed2,false,MONSTERS_AMOUNT); isHellSteed2[195]=true;
	memset(isHellSteed3,false,MONSTERS_AMOUNT); isHellSteed3[195]=true;

	memset(isRogue, false, MONSTERS_AMOUNT); isRogue[143] = true;

	for (long i = 0; i < MONSTERS_AMOUNT; i++) {
		CreatureSpellPowerMultiplier[i] = 1;
		CreatureSpellPowerDivider[i] = 1;
		CreatureSpellPowerAdder[i] = 0;
	}

	//majaczek hooks	
	
	//WriteHook((void*)0x004E6050, (void*)f_004E6050_Hook, HOOKTYPE_JUMP); //rogue_bad
	//WriteHook((void*), (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	
	WriteHook((void*) 0x4165c3, (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	WriteHook((void*) 0x4165de, (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	WriteHook((void*) 0x4167e8, (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	WriteHook((void*) 0x416803, (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	WriteHook((void*) 0x416fb4, (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	WriteHook((void*) 0x416fcf, (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	WriteHook((void*) 0x41719c, (void*)f_MoP_Rogue_Hook, HOOKTYPE_CALL);
	


	WriteHook((void*)0x75D2BB, (void*)f_75D2BB_Hook, HOOKTYPE_JUMP);

	WriteHook((void*)0x75D0F2,(void*)f_75D0F2_Hook, HOOKTYPE_JUMP);	
	WriteHook((void*)0x756B1F,(void*)f_756B1F_Hook, HOOKTYPE_JUMP);
	
	//WriteHook((void*)0x447ED9,(void*)f_447ED9_Hook, HOOKTYPE_JUMP);
	WriteHook((void*)0x4650dd,(void*)f_4650dd_Hook, HOOKTYPE_JUMP);
	WriteHook((void*)0x75C96C,(void*)f_75C96C_Hook, HOOKTYPE_JUMP);
	
	
	WriteHook((void*)0x75CA39,(void*)f_75CA39_Hook, HOOKTYPE_JUMP);	
	WriteHook((void*)0x75E8BA,(void*)f_75E8BA_Hook, HOOKTYPE_JUMP);
	WriteHook((void*)0x760723,(void*)f_760723_Hook, HOOKTYPE_JUMP);

	
	//spells
	  memset(spell_1_table,0x09,MONSTERS_AMOUNT);
	  memcpy(spell_1_table+0x0D,(void*)(*(int*)0x44825F),197-0x0D);
	
	  *(char*)0x44824A = 0;
	   *(int*)0x44825F = (int)spell_1_table;
	   
	  memset(spell_2_table,0x08,MONSTERS_AMOUNT);
	  memcpy(spell_2_table+0x0D,(void*)(*(int*)0x447475),197-0x0D);
	
	  *(char*)0x447467 = 0;
	   *(int*)0x447475 = (int)spell_2_table;

	   
	  memset(spell_3_table,0x03,MONSTERS_AMOUNT);
	  memcpy(spell_3_table+0x0D,(void*)(*(int*)0x421479),197-0x0D);
	
	  *(char*)0x42146F = 0;
	   *(int*)0x421479 = (int)spell_3_table;

	//skel
		for (int i=196; i!=MONSTERS_AMOUNT; i++) skeltrans[i]=56;
	    memcpy((char*)skeltrans,(void*)(0x7C3D00),197*4);
	    
	  	*(int*)(0x5664B5+3) = (int)skeltrans;
		*(int*)(0x56689D+3) = (int)skeltrans;
		*(int*)(0x566D79+3) = (int)skeltrans;
		*(int*)(0x566F3C+3) = (int)skeltrans;
		*(int*)(0x566FA4+3) = (int)skeltrans;
		*(int*)(0x566FD1+3) = (int)skeltrans;

		*(int*)(0x5664C9) = MONSTERS_AMOUNT;

	//upgtable

		memset((char*)upgtable, 0xFF, MONSTERS_AMOUNT*sizeof(int));
		
	  	*(int*)(0x724A92+3) = (int)upgtable;
		*(int*)(0x724AA8+3) = (int)upgtable;
		*(int*)(0x74EC7A+3) = (int)upgtable;
		*(int*)(0x74EC94+3) = (int)upgtable;
		*(int*)(0x74ED17+3) = (int)upgtable;
		*(int*)(0x74ED33+3) = (int)upgtable;
		*(int*)(0x75137D+1) = (int)upgtable;
		*(int*)(0x751EC4+1) = (int)upgtable;
		*(int*)(0x752E33+3) = (int)upgtable;
		*(int*)(0x7568F1+3) = (int)upgtable;
		

		*(int*)(0x751378+1) = (int)MONSTERS_AMOUNT*sizeof(int);
		*(int*)(0x751EB9+3) = (int)MONSTERS_AMOUNT*sizeof(int);
	//cast after attack ablilties
	  memset(aftercast_abilities_table,ACAST_DEFAULT,MONSTERS_AMOUNT);
	  memcpy(aftercast_abilities_table+0x16,(void*)(*(int*)0x440237),197-0x16);
	
	  *(char*)0x44022D = 0;
	   *(int*)0x440237 = (int)aftercast_abilities_table;
	  //memset((void*)0x440231,0x90,3);



	//special attacks
	  memset(attack_abilities_table,ATT_DEFAULT,MONSTERS_AMOUNT);
	  memcpy(attack_abilities_table+0x3F,(void*)(*(int*)0x440916),197-0x3F);
	
	  *(char*)0x440908 = 0;
	   *(int*)0x440916 = (int)attack_abilities_table;
	  //memset((void*)0x440909,0x90,3);


    //magic resistance
	  memset(magic_resistance_table,RESIST_DEFAULT,MONSTERS_AMOUNT);
	  memcpy(magic_resistance_table+0x10,(void*)(*(int*)0x44A4B3),197-0x10);
	
	  *(char*)0x44A4A9 = 0;
	   *(int*)0x44A4B3 = (int)magic_resistance_table;

	 //magic vulnerability by majaczek
	   memset(magic_vulnerability_table, VULN_DEFAULT, MONSTERS_AMOUNT);
	   memcpy(magic_vulnerability_table+0x20, (void*)(*(int*)0x44B197),197-0x20);

	   *(char*)0x44B189 = 0;
		*(int*)0x44B197 = (int)magic_vulnerability_table;

	//shot
		memset(special_missiles_table, 0, MONSTERS_AMOUNT);
		special_missiles_table[CREATURE_MAGOG] = 1;
		special_missiles_table[CREATURE_LICH] = 2;
		special_missiles_table[CREATURE_POWERLICH] = 2;
		special_missiles_table[CREATURE_DRACOLICH] = 2;

		WriteHook((void*)0x41ED5A,(void*)f41ED5A_Hook, HOOKTYPE_JUMP);

		WriteHook((void*)0x43F72C,(void*)f43F72C_Hook,HOOKTYPE_JUMP);

	//fire shield
		for (int i=0; i!=MONSTERS_AMOUNT; i++) fire_shield_table[i]=0;

		fire_shield_table[CREATURE_EFREET_SULTAN] = 0.2;
		//fire_shield_table[CREATURE_CHAMPION] = 1;

		WriteHook((void*)0x4225D6,(void*)f4225D6_Hook,HOOKTYPE_JUMP);
		WriteHook((void*)0x442E61,(void*)f442E61_Hook,HOOKTYPE_JUMP);
	
	//pegasi

	   
	//aftercast_abilities_table[197] = ACAST_ACID;
	//attack_abilities_table[197]=ATT_VAMPIRE;


	WriteHook((void*)0x0076735D, (void*)IsAngel3, HOOKTYPE_JUMP);

	FillTables();
	InitNecromancy();
}
