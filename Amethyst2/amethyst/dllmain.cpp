// dllmain.cpp: ���������� ����� ����� ��� ���������� DLL.


#include "stdafx.h"


extern void CreateNewTable(void);
extern void CreateAdditionalTables(void);
extern void NewMonstersMissiles(void);
extern void fix_amethyst_crexpmod(void);
//extern void InitNecromancy(void);

int all_creatures;
int conf_creatures;

extern char* experience_modifier_table;

void ForceTxtUnload(char* name) {
	auto h = GetModuleHandle(L"h3era.exe"); if (!h) h = GetModuleHandle(L"h3era HD.exe");
	if (!h) MessageBoxA(0, "cannot link to heroes3 executable", "debug", MB_OK);

	//MessageBoxA(0, "ForceTxtUnload 1", "debug", MB_OK);
	PTxtFile txt;
	LoadTxtFun__type LoadTxtFun = (LoadTxtFun__type)LoadTxtFun__ptr;
	UnloadTxtFun__type UnloadTxtFun = (UnloadTxtFun__type)UnloadTxtFun__ptr;
	//MessageBoxA(0, "ForceTxtUnload 2", "debug", MB_OK);
	txt = (*LoadTxtFun)(name); 
	//MessageBoxA(0, "ForceTxtUnload 3", "debug", MB_OK);
	if (!txt) return;	txt->ref_count = 1; 
	//MessageBoxA(0, "ForceTxtUnload 4", "debug", MB_OK);
	(*UnloadTxtFun)(txt);
	//MessageBoxA(0, "ForceTxtUnload 5", "debug", MB_OK);

}

void InitMainConfig(char *config_name)
{
	conf_creatures = 0x34; all_creatures = conf_creatures + 144;
	//MessageBoxA(0, config_name, "Detected Config: ",  0);
    char *buf;

        FILE *fdesc;
        if(fdesc=fopen(config_name,"r"))
        {
            //----------
            fseek (fdesc , 0 , SEEK_END);
            int fdesc_size=ftell(fdesc);
            rewind(fdesc);
            //----------
            buf=(char*)malloc(fdesc_size+1);
            fread(buf,1,fdesc_size,fdesc);
            buf[fdesc_size]=0;
            fclose(fdesc);
            //-----------
            char *c=strstr(buf,"Creatures=");
            if(c==0) {conf_creatures=0x34;}
            else     {conf_creatures=atoi(c+strlen("Creatures="))-144; }
            free(buf);
        }
        else
        {
			conf_creatures = 0x34;
            MessageBoxA(0,config_name, "Missed file: ",0);
        }    
		all_creatures = conf_creatures + 144;
}


// ThisIsNewGame 0x007caf20

/* __declspec(naked) */ void Amethyst_ResetExpTables()
{
	//reset experience tables
	
	//return;
	void(*CrExpSet__Clear)(void) = reinterpret_cast<void (*)(void)> (0x007186C1);
	int(*CrExpMod__Clear)(void) = reinterpret_cast<int (*)(void)> (0x0071A69E);
	int(*CrExpBon__Clear)(void) = reinterpret_cast<int (*)(void)> (0x0071AD24);

	CrExpSet__Clear();
	CrExpMod__Clear();
	CrExpBon__Clear();
}

void Amethyst_LoadExpTables()
{
	//reset experience tables

	//return;
	int(*CrExpSet__Load)(int) = reinterpret_cast<int(*)(int)> (0x007187ba);
	int(*CrExpMod__Load)(int) = reinterpret_cast<int(*)(int)> (0x0071A58C);
	int(*CrExpBon__Load)(int) = reinterpret_cast<int(*)(int)> (0x0071AAB5);

	//CrExpMod__Load(255); //any argument

}

void Amethyst_SaveExpTables()
{
	//reset experience tables

	//return;
	int(*CrExpSet__Save)(void) = reinterpret_cast<int(*)(void)> (0x00718837);
	int(*CrExpMod__Save)(void) = reinterpret_cast<int(*)(void)> (0x0071A638);
	int(*CrExpBon__Save)(void) = reinterpret_cast<int(*)(void)> (0x0071AB61);

	//CrExpMod__Load(); //any argument

}

// CrExpMod__Dummy        0x00791E80
// CrExpoSet__StopAllBF() 0x0072774E

// void __stdcall CopyExpModTables(TEvent* Event)
/*
__declspec(naked) void CopyExpModTables()
{
	__asm {
		
	}
	// memcpy((void*)experience_modifier_table, (void*)0x0085EB50, 20 * (197 + 5)); // not copies
}
*/

char* Hook_0x00705450_error_message = "crexpo reload failed !!! \n the game will now crash :( \n bye";
char* Hook_0x00705450_BUMP_message = "Hook_0x00705450";

__declspec(naked) void Hook_0x00705450() // at end of CalledBeforeTurn1New
{
	__asm {
		pusha

		mov  eax, 0x007186C1
		CALL eax
		mov  eax, 0x0071A69E
		CALL eax
		mov  eax, 0x0071AD24
		CALL eax

		/*
		mov  edi, esp
		PUSH 0
		PUSH 1
		PUSH Hook_0x00705450_BUMP_message
		mov	 eax, 0x0070FB63
		CALL eax
		mov  esp, edi

		mov  edi, esp
		PUSH 0
		PUSH 1
		PUSH Hook_0x00705450_BUMP_message
		mov	 eax, 0x0070FB63
		CALL eax
		mov  esp, edi

		mov edi, esp
		PUSH 18
		mov  ebx, 0x007187ba
		CALL ebx
		//ADD  ESP, 4
		TEST EAX, EAX
		JNE  z_error
		mov  esp, edi


		mov  edi, esp
		PUSH 0
		PUSH 1
		PUSH Hook_0x00705450_BUMP_message
		mov	 eax, 0x0070FB63
		CALL eax
		mov  esp, edi
		

		mov  edi, esp
		PUSH 18
		mov  ebx, 0x0071A58C
		CALL ebx
		//ADD  ESP, 4
		TEST EAX, EAX
		JNE  z_error
		mov  esp, edi


		mov  edi, esp
		PUSH 0
		PUSH 1
		PUSH Hook_0x00705450_BUMP_message
		mov	 eax, 0x0070FB63
		CALL eax
		mov  esp, edi


		mov  edi, esp
		PUSH 18
		mov  ebx, 0x0071AAB5
		CALL ebx
		//ADD  ESP, 4
		TEST EAX, EAX
		JNE  z_error
		mov  esp, edi
		*/

		mov edi, esp
		PUSH 0
		PUSH 1
		PUSH Hook_0x00705450_BUMP_message
		mov	 eax, 0x0070FB63
		CALL eax
		mov  esp, edi
		

		popa

		pop edi
		pop esi
		pop ebx
		mov esp, ebp
		pop ebp
		retn

		z_error:
			PUSH 0
			PUSH 1
			PUSH Hook_0x00705450_error_message
			mov	 eax, 0x0070FB63
			CALL eax

	}
	// memcpy((void*)experience_modifier_table, (void*)0x0085EB50, 20 * (197 + 5)); // not copies
}

//void __stdcall ReloadExperienceConfig(TEvent* Event)
void __stdcall ReloadExperienceConfig(void)
{
	//return;
	auto h= GetModuleHandle(L"h3era.exe"); if(!h) h=GetModuleHandle(L"h3era HD.exe");
	if (!h) MessageBoxA(0,"cannot link to heroes3 executable","debug",MB_OK);
	
	auto hEra = GetModuleHandle(L"era.dll"); 
	if(!hEra) MessageBoxA(0, "cannot link to era.dll", "debug", MB_OK);

	extern void asm_store(void); extern void asm_write(void); extern void asm_restore(void);
	asm_store(); asm_write();

	//typedef void(__stdcall *TForceTxtUnload) (char* Name); TForceTxtUnload ForceTxtUnload = nullptr;
	//auto ForceTxtUnload_ptr = GetProcAddress(hEra, "ForceTxtUnload");
	//if (!ForceTxtUnload_ptr) { MessageBoxA(0, "cannot get address of ForceTxtUnload from era.dll", "debug", MB_OK); return; }
	
	//ForceTxtUnload = (TForceTxtUnload)*ForceTxtUnload_ptr;

	LoadExpTXT__type LoadExpTXT = (LoadExpTXT__type) LoadExpTXT__ptr;
	//LoadExpTXT();

	CrExpoSet__Clear__type CrExpoSet__Clear = (CrExpoSet__Clear__type) CrExpoSet__Clear__ptr;
	CrExpMod__Clear__type CrExpMod__Clear = (CrExpMod__Clear__type) CrExpMod__Clear__ptr;
	CrExpBon__Clear__type CrExpBon__Clear = (CrExpBon__Clear__type) CrExpBon__Clear__ptr;

	auto z_entry = GetProcAddress(h,"entry");

	char debug[999] = ""; 
	/*
	sprintf_s(debug,"entry=%X %X=%X %X=%X %X=%X ", (int)z_entry,
		(int) CrExpoSet__Clear, CrExpoSet__Clear__ptr,
		(int)CrExpMod__Clear, CrExpMod__Clear__ptr, 
		(int)CrExpBon__Clear, CrExpBon__Clear__ptr);

	MessageBoxA(0,debug,"debug",MB_OK);

	sprintf_s(debug, "&val=%X \t val=%X \t *val=%X ", (int)&CrExpBon__Clear__ptr, 
		CrExpBon__Clear__ptr, *(int*)CrExpBon__Clear__ptr);
	MessageBoxA(0, debug, "debug", MB_OK);
	*/

	//ForceTxtUnload("ZCREXP.TXT");

	//MessageBoxA(0, "stage0.1", "debug", MB_OK);
	//(*LoadExpTXT)();
	(*CrExpoSet__Clear)();
	
	//MessageBoxA(0, "stage1.0", "debug", MB_OK);
	
	//ForceTxtUnload("CREXPMOD.TXT"); 
	//MessageBoxA(0, "stage1.1", "debug", MB_OK); 

	// *(unsigned char*)0x0071a748 = 0xeb;
	(*CrExpMod__Clear)();
	// *(unsigned char*)0x0071a748 = 0x74;

	//MessageBoxA(0, "stage2.0", "debug", MB_OK);

	//*(int*)CrExpBonFileLoaded__ptr = 0;
	//MessageBoxA(0, "CrExpBonFileLoaded__ptr = 0", "debug", MB_OK);

	//ForceTxtUnload("CREXPBON.TXT"); 
	//MessageBoxA(0,"crexpbon.txt Unloaded","debug",MB_OK);
	(*CrExpBon__Clear)();
	//MessageBoxA(0, "stage3.0", "debug", MB_OK);

	asm_restore();
	//MessageBoxA(0, "done", "debug", MB_OK);
}

void __stdcall ReloadExperienceConfig2(void)
{
	//return;
	auto h = GetModuleHandle(L"h3era.exe"); if (!h) h = GetModuleHandle(L"h3era HD.exe");
	if (!h) MessageBoxA(0, "cannot link to heroes3 executable", "debug", MB_OK);

	LoadExpTXT__type LoadExpTXT = (LoadExpTXT__type)LoadExpTXT__ptr;
	//LoadExpTXT();

	CrExpoSet__Clear__type CrExpoSet__Clear = (CrExpoSet__Clear__type)CrExpoSet__Clear__ptr;
	CrExpMod__Clear__type CrExpMod__Clear = (CrExpMod__Clear__type)CrExpMod__Clear__ptr;
	CrExpBon__Clear__type CrExpBon__Clear = (CrExpBon__Clear__type)CrExpBon__Clear__ptr;

	static bool __once__ = false;
	if (__once__) {

		(*CrExpoSet__Clear)();
		(*CrExpMod__Clear)();
		(*CrExpBon__Clear)();
		
		return;
	}
	__once__ = true;

	//MessageBoxA(0, "begin", " ReloadExperienceConfig2() ", MB_OK);
	//char debug[999] = "";

	ForceTxtUnload("ZCREXP.TXT");
	ForceTxtUnload("CREXPMOD.TXT");
	ForceTxtUnload("CREXPBON.TXT");

	//MessageBoxA(0, "1", "debug", MB_OK);
	(*LoadExpTXT)();
	(*CrExpoSet__Clear)();

	(*CrExpMod__Clear)();

	*(int*)CrExpBonFileLoaded__ptr = 0;
	(*CrExpBon__Clear)();

	//asm_restore();
	//MessageBoxA(0, "done", " ReloadExperienceConfig2() ", MB_OK);
}
BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		{	
			//fix_amethyst_crexpmod();
			//ConnectEra(); //added by majaczek

		
			/*if (FileExists("Mods\\Amethyst Upgrades\\Data\\amethyst.cfg"))
				InitMainConfig("Mods\\Amethyst Upgrades\\Data\\amethyst.cfg");
			else */ if (FileExists("Mods\\Knightmare Kingdoms\\Data\\amethyst.cfg"))
				InitMainConfig("Mods\\Knightmare Kingdoms\\Data\\amethyst.cfg"); 
			else InitMainConfig("Data\\amethyst.cfg");
			CreateNewTable();
			CreateAdditionalTables();

			fix_amethyst_crexpmod();
			//Amethyst_ResetExpTables();
			
			NewMonstersMissiles();
			
			// ConnectEra(); //added by majaczek
			/*
			RegisterHandler(Amethyst_ResetExpTables, "OnAfterErmInstructions");
			RegisterHandler(Amethyst_ResetExpTables, "OnAfterLoadGame");
			*/

			// RegisterHandler(CopyExpModTables, "OnAfterErmInstructions");
			/////WriteHook((void*) /*0x00705347*/ /*0x004cd5d0*/ 0x00705450, (void*)CopyExpModTables, HOOKTYPE_CALL); // at end of CalledBeforeTurn1New
			
			//WriteHook((void*)0x00705450, (void*)Hook_0x00705450, HOOKTYPE_JUMP); // at end of CalledBeforeTurn1New

			// Amethyst_ResetExpTables(); // Unknown (probably doesn't work here)
			
			//Amethyst_LoadExpTables(); // IDK why it doesn't work

			/*
			_asm{
				pushad
				mov  eax, 0x00703d57
				call eax
				popad
			};
			*/

			//ConnectEra(); //added by majaczek
			//ReloadExperienceConfig();
			//RegisterHandler(ReloadExperienceConfig, "OnCreateWindow");
			//RegisterHandler(ReloadExperienceConfig, "zzzzzzzzzzzz");

			//todo
			//ReloadExperienceConfig();
			//*(int*)CrExpBonFileLoaded__ptr = 0;
			//ReloadExperienceConfig2();

			break;
		}
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}

