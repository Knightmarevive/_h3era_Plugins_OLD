#include<Windows.h>

/* HookCode constants */

#define C_HOOKTYPE_JUMP  false
#define C_HOOKTYPE_CALL  true

#define C_OPCODE_JUMP   0xe9
#define C_OPCODE_CALL   0xe8
#define C_UNIHOOK_SIZE  5

#define C_MOP_DLLNAME   "Battery2.dll"

typedef unsigned char uchar;
typedef signed char   schar;
typedef void* Anyptr;
typedef unsigned long ulong;

#pragma pack(push, 1)
typedef struct THookRec {
	uchar Opcode;
	ulong Ofs;
	uchar padding[15];
} THookRec;
#pragma pack(pop)

/*
typedef struct {
	Anyptr proc, link;
} _PROCEDURE;
*/

HMODULE hBattery, hHeroes3;
static DWORD Temp, PointerOfSlotsTable, PointerOfAdditionalTable;
extern "C" {
	static char Error1[256] = "\"!!CA:D\" - wrong syntax";
	static char Error2[256] = "\"!!CA:D\" - A level of dwelling out of range (0...6)";
	static char Error3[256] = "\"!!CA:D\" - wrong slot number (0...3)";
	static char Error4[256] = "\"!!CA:D\" - A special building out of range (-1...-26)";
	static char Error5[256] = "\"!!CA:D\" - first argument out of range (0...6) or (-1...-26)";
	static char Error6[256] = "\"!!CA:D\" - A special building not supported";
	int Error1_ = (int)Error1; int Error2_ = (int)Error2; int Error3_ = (int)Error3;
	int Error4_ = (int)Error4; int Error5_ = (int)Error5; int Error6_ = (int)Error6;

	static  __declspec(naked) void _LogMsG_(char* msg) {
		MessageBoxA(0, msg, C_MOP_DLLNAME, MB_OK);
	}

	static char Log1[256] = "Debug - Active Custom Building"; int Log1_ = (int)Log1;
}

// fmcbl.htm									
schar AdditionalBuildingsList[] = {5,6,14,15,16, 17, 21, 22,23,26 }; //
schar AdditionalBuildingsAvailible[44];

 void zWriteAtCode(Anyptr P, Anyptr  Buf,long Count)
//Anyptr P, Buf;
//long Count;
{
	VirtualProtect(P, Count, PAGE_READWRITE, &Temp);
	CopyMemory(P, Buf, Count);
	VirtualProtect(P, Count, Temp, nullptr);
}


 void zHookCode(ulong P, ulong NewAddr, bool UseCall, ulong bytes = 5L)
//Anyptr P, NewAddr;
//boolean UseCall;
{
	THookRec HookRec;
	for (long i = 0; i < 15; ++i) 
		HookRec.padding[i] = 0x90;

	if (UseCall)
		HookRec.Opcode = C_OPCODE_CALL;
	else
		HookRec.Opcode = C_OPCODE_JUMP;
	HookRec.Ofs = NewAddr - P - C_UNIHOOK_SIZE;
	zWriteAtCode((Anyptr) P, (&HookRec), /* 5L */ bytes );
}


extern "C" __declspec(naked) __declspec(dllexport) void MOP_TOWN_RECRUIT_WINDOW()
{
	_asm {
		MOV EDX, ECX
		ADD EDX, 30
		MOV ECX, DS:[0x69954C]
		PUSH EBP
		MOV EBP, ESP
		PUSH -1
		PUSH 0x411CB6
		PUSH EAX
		SUB ESP, 0x22C
		PUSH EBX
		MOV EBX, ECX
		PUSH ESI
		PUSH EDI
		MOV EDI, EDX
		PUSH 0x5D42C4

		retn
	}
}


static __declspec(naked)  void Fort_hall()
{
	_asm {

		PUSH EBP
		MOV EBP, ESP
		PUSH EBX
		PUSH ESI
		PUSH EDI
		PUSHAD
		MOV EDI, ESI //
		MOV EAX, dword ptr DS : [0x69954C] //TownManager
		MOV DS : [0x836A18], EDX
		MOV DS : [0x83A86C], EAX
		PUSH 0x70DD45

		retn
	}
}


static __declspec(naked) void Horde()
{
	_asm {


		PUSH EBP
		MOV EBP, ESP
		PUSH EBX
		PUSH ESI
		PUSH EDI
		PUSHAD
		MOV ECX, [EBP + 8]
		MOV DS : [0x836A18], ECX
		mov edi, dword ptr DS : [0x6747B0]
		mov eCx, [ecx + 0x5C]
		IMUL ECX, 116
		MOV EDI, [EDI + ECX + 4] //
		MOV EAX, dword ptr DS : [0x69954C] //TownManager
		MOV DS : [0x83A86C], EAX
		PUSH 0x70DD45

		retn
	}
}

static __declspec(naked) void buildingclick_hook_2a(void) {
	_asm {
		CMP dword ptr DS:[0x00803288], 0x0
		JNZ entry_success
		popad
		mov eax, 0x7712b0
		call eax
		push 0x70dd2c
		retn 

		entry_success:
		mov ecx, edi
		sub ecx, 14
		cmp ecx, 0
		jl z_finish
		//pushad
		//and ecx, 0xff
		//mov cl, byte PTR DS : [ecx + AdditionalBuildingsAvailible]
		//cmp cl, 0xff
		//je z_finish
		mov edi, ecx

		mov eax, DWORD PTR  DS : [0x69954C]
		mov eax, [eax + 0x38]
		movsx eax, byte ptr[eax] //
		imul eax, 448 // 224

		imul edi, 32
		lea eax, [eax + edi]
		add eax, [PointerOfAdditionalTable]
		mov edx, dword ptr  DS : [0x836A18]
		mov ecx, [eax]
		cmp ecx, -1
		JLE z_default

		//mov ecx, Log1_
		//call _LogMsG_
//-----------------------------------------------------------------

			mov[edx + 0x5C], ecx //
			mov[edx + 0x50], ecx //
			lea ecx, [eax + 4]
			mov[edx + 0x6C], ecx
			mov ecx, [eax + 8]
			mov[edx + 0x60], ecx
			lea ecx, [eax + 12]
			mov[edx + 0x70], ecx
		mov ecx, [eax + 16]
			mov[edx + 0x64], ecx
			lea ecx, [eax + 20]
			mov[edx + 0x74], ecx
		mov ecx, [eax + 24]
			mov[edx + 0x68], ecx
			lea ecx, [eax + 28]
			mov[edx + 0x78], ecx

//-----------------------------------------------------------------
	z_recruit:
		popad                            

			POP        EDI
			POP        ESI
			POP        EBX
			MOV        ESP, EBP
			POP        EBP
			RET

		z_finish:
		popad
		push 0x0070dbaf
		ret


		z_default :
		popad

		POP        EDI
		POP        ESI
		POP        EBX
		MOV        ESP, EBP
		POP        EBP
		
		ret //todo

		/////
		sub esp, 4

		mov ECX, dword ptr DS:[0x006aaabc]
		push ecx

		//CALL       0x0060b0f0                                     
		mov eax, 0x0060b0f0
		call eax

		add esp,4
		mov ecx, ebx

		mov eax, 0x005d5810
		call eax

		POP        EDI
		POP        ESI
		POP        EBX
		POP        EBP
			
		// sub esp, 0

		//ret 4

		JMP DWORD PTR DS : [0x5D4EB4]
		
	}
}

static __declspec(naked) void buildingclick_hook_1(void) {
	_asm {
		pushad
		and ecx, 0xff
		mov cl, byte PTR DS : [ecx + AdditionalBuildingsAvailible]
		cmp cl, 0
		jl z_finish
		mov edi, ecx

		mov eax, DWORD PTR  DS : [0x69954C]
		mov eax, [eax + 0x38]
		movsx eax, byte ptr[eax] //
		imul eax, 224

		imul edi, 32
		lea eax, [eax + edi]
		add eax, [PointerOfAdditionalTable]
		mov edx, dword ptr  DS : [0x836A18]
		mov ecx, [eax]
		cmp ecx, -1
		JLE z_finish 

		//mov ecx, Log1_
		//call _LogMsG_
		

		/*
		mov[edx + 0x5C], ecx //
		mov[edx + 0x50], ecx //
		lea ecx, [eax + 4]
		mov[edx + 0x6C], ecx
		// _label__not_change0 :
		mov ecx, [eax + 8]
			cmp ecx, -2
			JLE _label__not_change1
			mov[edx + 0x60], ecx
			lea ecx, [eax + 12]
			mov[edx + 0x70], ecx
			_label__not_change1 :
		mov ecx, [eax + 16]
			cmp ecx, -2
			JLE _label__not_change2
			mov[edx + 0x64], ecx
			lea ecx, [eax + 20]
			mov[edx + 0x74], ecx
			_label__not_change2 :
		mov ecx, [eax + 24]
			cmp ecx, -2
			JLE _label__not_change3
			mov[edx + 0x68], ecx
			lea ecx, [eax + 28]
			mov[edx + 0x78], ecx
			_label__not_change3:
		*/

			z_recruit:
			popad
			POP     EDI
			POP     ESI
			POP     EBX
			MOV     ESP, EBP
			//sub		esp, 4  //injected
			POP     EBP
			//push 0x00
			RET


		//-------------------------------------------------
		//--todo--
		//MOV  EAX, dword ptr DS : [0x00789030]// [->FUN_004b0770] = 004b0770
		//MOV  dword ptr DS : [0x007c8554], EAX // = ? ?
		//PUSH dword ptr DS : [0x00836a18] // = ? ?
		//CALL dword ptr DS : [0x007c8554]
		//-------------------------------------------------

		z_finish:
		popad
		MOV EAX, DWORD PTR DS : [ESI + 0x14]
		MOV EDX, DWORD PTR DS : [ESI + 0x10]
		push 0x74f4CE
		retn
	}
}

static __declspec(naked) void allslots_realize()
{
	_asm {


		mov eax, DWORD PTR  DS : [0x69954C]
		mov eax, [eax + 0x38]
		movsx eax, byte ptr [eax] //
		imul eax, 224 //
		//--------------
		cmp EDI, 14
		jge z_finish
		//--------------
		Cmp EDI, 7
		JL _nonupgrade
		sub edi, 7
		_nonupgrade:
		imul edi, 32
		lea eax, [eax + edi]
		add eax, [PointerOfSlotsTable]
		mov edx, dword ptr  DS : [0x836A18]
		mov ecx, [eax]
		cmp ecx, -2
		JLE _label__not_change0
		mov[edx + 0x5C], ecx //
		mov[edx + 0x50], ecx //
		lea ecx, [eax + 4]
		mov[edx + 0x6C], ecx
		_label__not_change0:
		mov ecx, [eax + 8]
		cmp ecx, -2
		JLE _label__not_change1
		mov[edx + 0x60], ecx
		lea ecx, [eax + 12]
		mov[edx + 0x70], ecx
		_label__not_change1:
		mov ecx, [eax + 16]
		cmp ecx, -2
		JLE _label__not_change2
		mov[edx + 0x64], ecx
		lea ecx, [eax + 20]
		mov[edx + 0x74], ecx
		_label__not_change2:
		mov ecx, [eax + 24]
		cmp ecx, -2
		JLE _label__not_change3
		mov[edx + 0x68], ecx
		lea ecx, [eax + 28]
		mov[edx + 0x78], ecx
		_label__not_change3:
		z_finish:
		pop edi
		pop esi
		pop ebx
		mov esp, ebp
		pop ebp

		retn
	}
}


static __declspec(naked)  void Reset_Table()
{
	//_LogMsG_("Reset_Table");
	_asm {

		MOV EAX, 0x72C8B1
		CALL EAX //
		MOV ECX, 2688 // /4
		MOV EAX, -2 //  -2
		MOV EDI, [PointerOfSlotsTable] // 
		REP STOSD //
		//PUSH 0x74C7E2 //


		//MOV EAX, 0x72C8B1
		//CALL EAX //
		MOV ECX, 5376 // 2688  // /4
		MOV EAX, -1 //  -1
		MOV EDI, [PointerOfAdditionalTable] // 
		REP STOSD //
		PUSH 0x74C7E2 //


		retn
	}
}


static __declspec(naked)  void Save_Table()
{
	_asm {


		PUSH 10752
		PUSH[PointerOfSlotsTable]
		MOV EAX, 0x704062
		CALL EAX
		ADD ESP, 8
		TEST EAX, EAX
		JNE _label_Error


		PUSH 21504 // 10752 //9216
		PUSH[PointerOfAdditionalTable]
		MOV EAX, 0x704062
		CALL EAX
		ADD ESP, 8
		TEST EAX, EAX
		JE _label__Norm

		_label_Error:
		MOV EAX, 0x7712B0 //
		CALL EAX
		MOV EAX, 1
		PUSH 0x7515F8
		RET
		_label__Norm:
		PUSH 0x7D000
		PUSH 0x75102B

		retn
	}
}


static __declspec(naked)  void Load_Table()
{
	_asm {


		PUSH 10752
		PUSH[PointerOfSlotsTable]
		MOV EAX, 0x7040A7
		CALL EAX
		ADD ESP, 8
		TEST EAX, EAX
		JNE _label_Error


		PUSH 21504 //10752 //9216
		PUSH[PointerOfAdditionalTable]
		MOV EAX, 0x7040A7
		CALL EAX
		ADD ESP, 8
		TEST EAX, EAX
		JE _label__Norm


		_label_Error:
		MOV EAX, 0x7712B0 //
		CALL EAX
		MOV EAX, 1
		PUSH 0x752591
		RET
		_label__Norm:
		PUSH 0x7D000
		PUSH 0x7517F4

		retn

	}
}


static __declspec(naked)  void CA_D_realize()
{

	//_LogMsG_("CA_D_realize");
	_asm {


		mov eax, [ebp - 0x38]
		CMP EAX, 'D' // 
		JNZ _label__not_CA_D
		cmp dword ptr [ebp + 0x0C], 4
		JE _label__norm_number_of_param
		PUSH Error1_
		PUSH 0x70DFB8
		RET
		_label__norm_number_of_param:
		PUSH 0 //  
		PUSH dword ptr [EBP + 0x14]
		PUSH 4 // 
		LEA EAX, [EBP - 4] // 
		PUSH EAX
		MOV EAX, 0x74195D // 
		CALL EAX
		ADD ESP, 0x10
		// CMP ECX, 6
//-------------------------------------
		cmp ecx, 0
		//jl _label__ERROR_1
		jl _label__negative
		cmp ecx, 6
		jle _label__norm_dwelling_level
		jmp _label__ERROR_1
		//sub ecx, 16
		_label__negative:
		neg ecx
			//todo
		//cmp ecx, 6
		//jl z_label__ERROR_5
		cmp ecx, 26
		jg z_label__ERROR_4
		and ecx, 0xFF 
		mov cl, DS:[AdditionalBuildingsAvailible+ecx]
		cmp cl, 44
		jae z_label__ERROR_6
		

		jmp __label_custom_building_

		z_label__ERROR_4:
		PUSH Error4_
		PUSH 0x70DFB8
		RET

		z_label__ERROR_5:
		PUSH Error5_
		PUSH 0x70DFB8
		RET

		z_label__ERROR_6:
		PUSH Error6_
		PUSH 0x70DFB8
		RET
//-------------------------------------
		// JG _label__ERROR_1
		// TEST ECX, ECX
		// JL _label__ERROR_1
		//JMP _label__norm_dwelling_level
		_label__ERROR_1:
		PUSH Error2_
		PUSH 0x70DFB8
		RET
		_label__norm_dwelling_level:
		MOV EDX, [EBP - 0x10] // 
		MOVSX EDX, BYTE ptr [EDX] // 
		IMUL EDX, 224 // 
		ADD edx, [PointerOfSlotsTable]
		IMUL ECX, 32 // 
		ADD EDX, ECX
		MOV[EBP - 4], EDX
		PUSH 1
		PUSH dword ptr [EBP + 0x14]
		PUSH 4
		LEA EAX, [EBP - 0x18]
		PUSH EAX
		MOV EAX, 0x74195D // 
		CALL EAX
		ADD ESP, 0x10
		CMP ECX, 3
		JG _label__ERROR_2
		TEST ECX, ECX
		JL _label__ERROR_2
		JMP _label__norm_slot_number
		_label__ERROR_2:
		PUSH Error3_
		PUSH 0x70DFB8
		RET
		_label__norm_slot_number:
		IMUL ECX, 8
		ADD[EBP - 4], ECX
		PUSH 2
		PUSH dword ptr [EBP + 0x14]
		PUSH 4
		PUSH dword ptr [EBP - 4] //  
		MOV EAX, 0x74195D // 
		CALL EAX
		ADD ESP, 0x10
		PUSH 3
		PUSH dword ptr [EBP + 0x14]
		PUSH 4
		MOV eax, [EBP - 4]
		ADD EAX, 4 //  +4  
		PUSH EAX
		MOV EAX, 0x74195D //  4- 
		CALL EAX
		ADD ESP, 0x10
		PUSH 0x70E984
		RET

//-------------------------------------
__label_custom_building_:

		MOV EDX, [EBP - 0x10] // 
			MOVSX EDX, BYTE ptr[EDX] // 
			IMUL EDX, 448 // 224 // 
			ADD edx, [PointerOfAdditionalTable]
			IMUL ECX, 32 // 
			ADD EDX, ECX
			MOV[EBP - 4], EDX
			PUSH 1
			PUSH dword ptr[EBP + 0x14]
			PUSH 4
			LEA EAX, [EBP - 0x18]
			PUSH EAX
			MOV EAX, 0x74195D // 
			CALL EAX
			ADD ESP, 0x10
			CMP ECX, 3
			JG _label__ERROR_2
			TEST ECX, ECX
			JL _label__ERROR_2


			IMUL ECX, 8
			ADD[EBP - 4], ECX
			PUSH 2
			PUSH dword ptr[EBP + 0x14]
			PUSH 4
			PUSH dword ptr[EBP - 4] //  
			MOV EAX, 0x74195D // 
			CALL EAX
			ADD ESP, 0x10
			PUSH 3
			PUSH dword ptr[EBP + 0x14]
			PUSH 4
			MOV eax, [EBP - 4]
			ADD EAX, 4 //  +4  
			PUSH EAX
			MOV EAX, 0x74195D //  4- 
			CALL EAX
			ADD ESP, 0x10
			PUSH 0x70E984
			RET

//-------------------------------------

		_label__not_CA_D:
		sub eax, 0x42
		PUSH 0x70DF8F

		retn
	}
}


static __declspec(naked)  void Fort_Trigger()
{
	_asm {


		mov edx, 0x0E
		PUSHAD
		SETE BYTE ptr DS : [0xA4AAFC] // 
		MOV edi, 0x8912A8
		mov esi, ebx
		push esi // 
		push edi
		mov ecx, 6
		REP MOVSD // 
		PUSH 88888 //  ERM- 
		MOV EAX, 0x74CE30
		CALL EAX
		ADD ESP, 4
		pop edi
		pop esi
		XCHG ESI, EDI // 
		mov ecx, 6
		REP MOVSD // 
		POPAD
		TEST BYTE ptr DS : [0xA4AAFC], 1 //  CM:R
		JNZ _label__standart
		PUSH 0x5DD64A
		RET
		_label__standart:
		PUSH 0x5DD3D0

		retn
	}
}

long buildingclick_hook_2b_EDI_new, buildingclick_hook_2b_EDI_old;
static __declspec(naked)  void buildingclick_hook_2b()
{
	_asm {
		pushad
		cmp eax, 2
		jne z_finish
		MOV EDI, DWORD PTR DS : [ESI + 8]
		TEST EDI, EDI
		JL z_finish
		CMP EDI, 0x2B
		JG z_finish

		entry_success :
		mov buildingclick_hook_2b_EDI_old, EDI
		mov ecx, edi
		cmp ecx, 0
		jl z_finish
		//pushad
		and ecx, 0xff
		mov cl, byte PTR DS : [ecx + AdditionalBuildingsAvailible]
		cmp cl, 0xff
		je z_finish
			
			
			//todo
			mov edi, ecx
			
			mov eax, DWORD PTR  DS : [0x69954C]
			mov eax, [eax + 0x38]
			movsx eax, byte ptr[eax] //
			imul eax, 448 // 224

			imul edi, 32
			lea eax, [eax + edi]
			//lea eax, [eax + buildingclick_hook_2b_EDI_old]
			add eax, [PointerOfAdditionalTable]
			//mov edx, dword ptr  DS : [0x836A18]
			mov eax, [eax]
			cmp eax, -1
			jle z_disabled
			

		add ecx, 44
		mov edi, ecx
		jmp z_ready

		z_disabled:
		popad
		JMP DWORD PTR DS : [EAX * 4 + 0x5D4EAC]

		z_finish:
		popad
		JMP DWORD PTR DS : [EAX * 4 + 0x5D4EAC]

		z_ready:
		mov buildingclick_hook_2b_EDI_new,EDI
		popad
		MOV EAX, DWORD PTR DS : [ESI + 0x14]
		MOV EDX, DWORD PTR DS : [ESI + 0x10]
		XOR EDI, EDI
		LEA EAX, [EAX * 4 + EAX]
		LEA ECX, [EAX * 4 + EAX]
		SHL ECX, 5
		ADD ECX, EDX
		MOV EDX, DWORD PTR DS : [EBX + 0x118]
		MOV EAX, DWORD PTR DS : [EDX + 0x50]
		MOV DI, WORD PTR DS : [ECX * 2 + EAX]
		DEC EDI
		MOV DWORD PTR SS : [EBP - 0x10], EDI
		//mov EDI, buildingclick_hook_2b_EDI
		//---

		//todo
		
		//---
		mov EDI, buildingclick_hook_2b_EDI_new
		push 0x5d42db
		retn
	}
}
extern "C" __declspec(dllexport) BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{

	if (reason == DLL_PROCESS_ATTACH)
	{
		//_PROCEDURE TEMP1;
		// Anyptr TEMP1;


		//MessageBoxA(0, "entry", C_MOP_DLLNAME, MB_OK);

		hBattery = GetModuleHandle(C_MOP_DLLNAME);
		DisableThreadLibraryCalls(hBattery);

		hHeroes3 = GetModuleHandle("h3era.exe"); if(!hHeroes3) hHeroes3 = GetModuleHandle("h3era HD.exe");

		if (!hBattery || !hHeroes3) _LogMsG_("Cannot link to the game !!!");

		for (schar i = 0;  i < 44;  i++) AdditionalBuildingsAvailible[i] = -1;
		for (schar i = 0;  i < 10;  i++) AdditionalBuildingsAvailible[ AdditionalBuildingsList [i] ] = i;
		for (schar i = 0;  i <= 4;  i++) AdditionalBuildingsAvailible[i] = 13;
		for (schar i = 7;  i <= 9;  i++) AdditionalBuildingsAvailible[i] = 12;
		for (schar i = 10; i <= 13; i++) AdditionalBuildingsAvailible[i] = 11;
		//MessageBoxA(0, "" , "" , MB_OK);

		//MessageBoxA(0, "begin", C_MOP_DLLNAME, MB_OK);

		//PASCAL_MAIN(argc, argv);
		//TEMP1 = (Anyptr)Fort_hall;
		zHookCode(0x5dd2fcL, (ulong)  Fort_hall, C_HOOKTYPE_CALL);
		//TEMP1 = (Anyptr)Horde;
		zHookCode(0x5d4271L, (ulong)  Horde, C_HOOKTYPE_CALL);
		//TEMP1 = (Anyptr)allslots_realize;
		zHookCode(0x70dd2cL, (ulong)  allslots_realize, C_HOOKTYPE_JUMP);

		//zHookCode(0x74f4c8L, (ulong) buildingclick_hook_1, C_HOOKTYPE_JUMP, 6);

		zHookCode(0x70DB9bL, (ulong)buildingclick_hook_2a, C_HOOKTYPE_JUMP, 10);
		zHookCode(0x5d3876L, (ulong)buildingclick_hook_2b, C_HOOKTYPE_JUMP, 7);


		_asm {
			pushad

			PUSH 4
			PUSH 0x1000
			PUSH 10752 //  = 8   ( ) * 4  * 7   * 48  
			PUSH 0
			CALL VirtualAlloc
			PUSH 4
			PUSH 0x1000
			PUSH 10752 // 
			PUSH EAX
			CALL VirtualAlloc
			MOV DS:[PointerOfSlotsTable], EAX

			popad
		}


		_asm {
			pushad

			PUSH 4
			PUSH 0x1000
			PUSH 21504 //  10752  //  = 8 * 4  * 14  * 48  
			PUSH 0
			CALL VirtualAlloc
			PUSH 4
			PUSH 0x1000
			PUSH 21504 //  10752 //9216 // 
			PUSH EAX
			CALL VirtualAlloc
			MOV DS : [PointerOfAdditionalTable], EAX

			popad
		}


		//TEMP1 = (Anyptr)Reset_Table;
		zHookCode(0x74c7ddL, (ulong)  Reset_Table, C_HOOKTYPE_JUMP);
		//TEMP1 = (Anyptr)Save_Table;
		zHookCode(0x751026L, (ulong)  Save_Table, C_HOOKTYPE_JUMP);
		//TEMP1 = (Anyptr)Load_Table;
		zHookCode(0x7517efL, (ulong)  Load_Table, C_HOOKTYPE_JUMP);
		//TEMP1 = (Anyptr)CA_D_realize;
		zHookCode(0x70df89L, (ulong)  CA_D_realize, C_HOOKTYPE_JUMP);
		//TEMP1 = (Anyptr)Fort_Trigger;
		zHookCode(0x5dd3cbL, (ulong)   Fort_Trigger, C_HOOKTYPE_JUMP);


		//MessageBoxA(0, "loaded", C_MOP_DLLNAME, MB_OK);
		//exit(EXIT_SUCCESS);
	}

	return true;
}



/* End. */