 #include "pch.h"

#include "heroes.h"
#include "patcher_x86.hpp"
#include "H3DlgItem.hpp"

#include "call_convention.hpp"

typedef void(*voidfun)(void);
voidfun DLG_GetItem = (voidfun) 0x5FF5B0;
voidfun DLG_Refresh = (voidfun) 0x5FF5E0;
voidfun ret_4df7f8  = (voidfun) 0x4df7f8;

H3BaseDlg* current_HERO_DLG = 0;


/*
__declspec(naked) void Renumber_HERO_Dialog_SS_icons(void) {
	_asm {
		pushad

		// get dialogue
		mov eax, DS:[0x6992D0]
		add eax, 0x54
		mov ebx, DS:[eax]
		mov current_DLG, ebx

		push 0x4f
		push current_DLG
		call DLG_GetItem

		push 65535
		push -65535
		push 1
		push current_DLG

		popad
		ret
	}
}
*/

int show_hero_SS[10] = {0};

int current_hero = -1;
int get_new_skill_frame_number(char *arg_hero, int position) {
	if (!arg_hero) return 5;
	HERO* hero = (HERO*)arg_hero;


	for (int i = 0; i < 28; i++) {
		if (hero->SShow[i] == position) {
			return 16 + 16 * i +  hero->SSkill[i];

		}
	}

	return 0;
}

int get_new_skill_frame_number_wrapper(int hero, int position) {
	
	if (hero < 0) return 15;
	int ret = get_new_skill_frame_number
		(GetHeroRecord(hero), position);

	if (ret < 0 || ret> 784) return 0;

	return ret;
}

int HERO_Screen_Calc_value= 1;
__declspec(naked) void HERO_Screen_Calc(void) {
	_asm {
		pushad
		push ebx
		push current_hero
		call get_new_skill_frame_number_wrapper
		pop ebx
		mov HERO_Screen_Calc_value, ebx
		popad
		mov ebx, HERO_Screen_Calc_value
		ret 
	}
}

void  fill_show_current_hero_SS(void) {
	for (int i = 1; i < 9;i++) {
		show_hero_SS[i] = get_new_skill_frame_number_wrapper(current_hero,i);
	}
}


__declspec(naked) void asm_HERO_Screen_Preset(void) {
	_asm {
		pushad
		mov current_hero, 136
		call fill_show_current_hero_SS
		popad
		jmp ret_4df7f8
	}
}


void HERO_Screen_Preset(void) {
	current_hero = 136;
	fill_show_current_hero_SS();
}


int __stdcall hook_HERO_Screen_Preset(LoHook* h, HookContext* c) {

	//current_hero = 136;
	fill_show_current_hero_SS();
	return NO_EXEC_DEFAULT;
}


H3DlgDef* current_ICON;
void __stdcall HERO_DLG_change_frames(void) {


	// THISCALL_4(VOID, current_HERO_DLG->vtable->redrawDlg, current_HERO_DLG, TRUE, -65535, 65535);
	
	for(int i =1; i<9; i++)
	{
		current_ICON = THISCALL_2(H3DlgDef*, 0x5FF5B0, current_HERO_DLG, 0x44e+i);
		if (current_ICON) { 
			current_ICON->defFrame = show_hero_SS[i]; 
			// THISCALL_1(VOID, current_ICON->vTable->draw, current_ICON); 
		}
	}


	// THISCALL_4(VOID, current_HERO_DLG->vtable->redrawDlg, current_HERO_DLG, TRUE, -65535, 65535);

}

int __stdcall hook_HERO_Screen_Refresh(LoHook* h, HookContext* c) {
	current_HERO_DLG = (H3BaseDlg*) c->ebx;


	for (int i = 0; i < 10; i++)
		show_hero_SS[i] = 0;

	fill_show_current_hero_SS();
	HERO_DLG_change_frames();
	return NO_EXEC_DEFAULT;
}

int __stdcall hook_HERO_zecsk82a(LoHook* h, HookContext* c) {
	int * SS_dialog = (int*)(char*)(c->ebp + 8);
	if(*SS_dialog != 20) return EXEC_DEFAULT;

	int* SS_return = (int*) (char*) (c->ebp + 12);
	int* SS_chosen = (int*) (char*) (c->esp + 0x14); // c->edx; // c->eax;
	int* SS_level  = (int*) (char*) (c->esp + 0x10);


	*SS_return = 10 + 10 * *SS_chosen + *SS_level;// hero->SSkill[i];
	return EXEC_DEFAULT;
}


int __stdcall hook_HERO_zecsk82b(LoHook* h, HookContext* c) {
	int* SS_dialog = (int*)(char*)(c->ebx + 0);
	if (*SS_dialog != 20) return EXEC_DEFAULT;

	int* SS_return = (int*)(char*)(c->ebx + 4);
	int* SS_chosen = (int*)(char*)(c->esp + 0x10); // c->edx; // c->eax;
	int* SS_level  = (int*)(char*)(c->esp + 0xc);


	*SS_return = 10 + 10 * *SS_chosen + *SS_level;// hero->SSkill[i];
	return EXEC_DEFAULT;
}