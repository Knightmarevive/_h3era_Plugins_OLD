#include "MyTypes.h"
#include "H3DlgItem.hpp"
#include <cstdio>

char lvl_0[512] = "None";
char lvl_1[512] = "Level 1";
char lvl_2[512] = "Level 2";
char lvl_3[512] = "Level 3";
char lvl_4[512] = "Level 4";
char lvl_5[512] = "Level 5";
char lvl_6[512] = "Level 6";
char lvl_7[512] = "Level 7";
char lvl_8[512] = "Level 8";
char lvl_9[512] = "Level 9";
char lvl_A[512] = "Level 10";
char lvl_B[512] = "Level 11";
char lvl_C[512] = "Level 12";
char lvl_D[512] = "Level 13";
char lvl_E[512] = "Level 14";
char lvl_F[512] = "Level 15";

// H3SecondarySkillInfoNew SS_Descriptions[28];

char* Skill_Levels[] = {
	lvl_0, lvl_1,lvl_2, lvl_3,lvl_4, lvl_5, lvl_6, lvl_7,lvl_8, lvl_9, lvl_A, lvl_B, lvl_C, lvl_D, lvl_E, lvl_F
};

H3SecondarySkillInfoNew newSkillsDescriptions[28] = {};
H3SecondarySkillInfo* oldSkillsDescriptions = (H3SecondarySkillInfo*)  0x00698d88;

replace_dword SS_text_dwords[] = {
	{0x005f0182 + 2, (unsigned int)(Skill_Levels + 1)}, // mov ecx, dword ptr ds:[0x006A75D8]
	{0x005f1390 + 1, (unsigned int)(Skill_Levels + 1)}, // mov eax, dword ptr ds:[0x006A75D8]
	{0x005f0d04 + 2, (unsigned int)(Skill_Levels + 1)}, // mov edx, dword ptr ds:[0x006A75D8]
	{0x005efe11 + 2, (unsigned int)(Skill_Levels + 1)}, // mov ecx, dword ptr ds:[0x006A75D8]
	{0x005f0fed + 2, (unsigned int)(Skill_Levels + 1)}, // mov ecx, dword ptr ds:[0x006A75D8]

	{0x004dadc3 + 3, (unsigned int)(Skill_Levels + 1)},
	{0x004daddc + 3, (unsigned int)(Skill_Levels + 1)},
	{0x004F5D1B + 3, (unsigned int)(Skill_Levels + 1)},
	{0x004F9304 + 3, (unsigned int)(Skill_Levels + 1)},
	{0x004F933E + 3, (unsigned int)(Skill_Levels + 1)},
	{0x004F96E0 + 3, (unsigned int)(Skill_Levels + 1)},

	/*
	{0x075EED00 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075EEF00 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075EF100 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075Ef200 + 0, (unsigned int)(Skill_Levels + 1)},
	{0x075Ef300 + 0, (unsigned int)(Skill_Levels + 1)},
	*/

	//// 0x006A75D4
	{0x004847B1 + 3, (unsigned int)(Skill_Levels + 0)}, // mov edx, dword ptr ds:[ecx*4+0x6A75D4]
	{0x004DBE9A + 3, (unsigned int)(Skill_Levels + 0)}, // mov eax, dword ptr ds:[edx*4+0x6A75D4]
	{0x004E225A + 3, (unsigned int)(Skill_Levels + 0)}, // mov edi, dword ptr ds:[ecx*4+0x6A75D4]
	{0x005215BD + 3, (unsigned int)(Skill_Levels + 0)}, // mov eax, dword ptr ds:[edx*4+0x6A75D4]
	{0x005B0EB1 + 3, (unsigned int)(Skill_Levels + 0)}, // mov ecx, dword ptr ds:[eax*4+0x6A75D4]
	{0x005B9785 + 2, (unsigned int)(Skill_Levels + 0)}, // mov dword ptr ds:[ecx+0x6A75D4], edx

	/*
	{0x0040DCDA + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x00484797 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004A7DA9 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004A7DDE + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004A7E34 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004DAD9E + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004DBE7D + 2,(unsigned int)(newSkillsDescriptions + 0)},
	// {0x004DE7E8 + 2,(unsigned int)(&newSkillsDescriptions + 0)}, //missing
	{0x004E21F9 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F1536 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F5D85 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F9323 + 1,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F95A9 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F9623 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F96E7 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004F9816 + 1,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004FA074 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x004FA0BA + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x0051F53B + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005215A0 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005B0367 + 1,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005B0E62 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005B0E91 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005EF874 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005EFD89 + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005F0CFB + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005F0FCD + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x005F136D + 2,(unsigned int)(newSkillsDescriptions + 0)},
	{0x00714B1F + 2,(unsigned int)(newSkillsDescriptions + 0)},
	*/

	{0x67DCF0,(unsigned int)(newSkillsDescriptions + 0)},

	{0,0}
};




replace_byte SS_text_bytes[] = {
	{0x0040DCE0 + 2, 0x6},
	{0x004847A1 + 2, 0x6},
	{0x004A7DB5 + 2, 0x6},
	{0x004A7DE9 + 2, 0x6},
	{0x004A7E3C + 2, 0x6},
	{0x004DADA6 + 2, 0x6},
	{0x004DBE85 + 2, 0x6},
	//{0x???????? + 2, 0x6},
	{0x004E2201 + 2, 0x6},
	// {} // missing
	// {0x004F5D94 + 2, 0x6},
	{0x004F932B + 2, 0x6},
	// {} // missing
	// {} // missing
	{0x004F96DD + 2, 0x6},
	// {} // missing
	// {} // missing
	// {} // missing
	// {} // missing
	{0x005215A8 + 2, 0x6},
	// {} // missing
	{0x005B0E6A + 2, 0x6},
	{0x005B0E99 + 2, 0x6},
	// {} // missing
	{0x005EFD93 + 2, 0x6},
	{0x005F0CF5 + 2, 0x6},
	{0x005F0FE1 + 2, 0x6},
	{0x005F1383 + 2, 0x6},
	{0x00714B2A + 2, 0x6},

	{0,0}

};


void SS_text_clean(void) {
	char buf[4096];
	H3SecondarySkillInfo *old = oldSkillsDescriptions;
	for (int i = 0; i < 28; i++) {
		// sprintf(buf, "Skill %i name", i);
		char* zname = new char[4096];
		// strcpy(zname,buf);
		// newSkillsDescriptions[i].name = zname;
		newSkillsDescriptions[i].name = zname;
		strcpy(zname, old[i].name );
		for (int j = 0; j < 3; j++) {

			char* zdesc = new char[4096];
			newSkillsDescriptions[i].description[j] = zdesc;
			strcpy(zdesc , old[i].description[j]);
		}
		for (int j = 3; j < 15; j++) {
			char* zdesc = new char[4096];
			sprintf(buf, "Skill %i description level %i", i, j+1);
			strcpy(zdesc, buf);
			newSkillsDescriptions[i].description[j] = zdesc;
		}

	}
}