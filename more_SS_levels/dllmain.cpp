// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include "patcher_x86.hpp"
#include "MyTypes.h"

#include "era.h"

#define PINSTANCE_MAIN "more_SS_levels"

Patcher* globalPatcher;
PatcherInstance* Zecondary_Skills;

void show_HERO_SS_icons(void);
void show_HERO_SS_icons_original(void);


void SS_text_clean(void);
replace_byte  SS_text_bytes[];
replace_dword SS_text_dwords[];
extern int current_hero;

void fill_show_current_hero_SS(void);
void HERO_Screen_Preset(void);
void asm_HERO_Screen_Preset(void);
int __stdcall hook_HERO_Screen_Preset(LoHook* h, HookContext* c);
int __stdcall hook_HERO_Screen_Refresh(LoHook* h, HookContext* c);
int __stdcall hook_HERO_zecsk82a(LoHook* h, HookContext* c);
int __stdcall hook_HERO_zecsk82b(LoHook* h, HookContext* c);

void calc_pre_msgbox(void);
void calc_in_msgbox(void);

void __stdcall Z_OnPreHeroScreen(PEvent e) {
	current_hero = ErmX[1];
	// fill_show_current_hero_SS();

	// void HERO_Screen_Preset(void);

}

void __stdcall Z_OnBeforeHeroInteraction(PEvent e) {

}

void __stdcall Z_OnAfterWog(PEvent e) {

}
void __stdcall Z_OnGameEnter(PEvent e) {


}

void Z_Replace_SS_text_dwords(void) {
	SS_text_clean();

	for (int i = 0; SS_text_dwords[i].address; i++)
		Zecondary_Skills->WriteDword(SS_text_dwords[i].address, SS_text_dwords[i].new_dword);

	for (int i = 0; SS_text_bytes[i].address; i++)
		Zecondary_Skills->WriteByte(SS_text_bytes[i].address, SS_text_bytes[i].new_byte);
}



void __stdcall Z_Replace_SS_text_dwords(PEvent e) {
	Z_Replace_SS_text_dwords();
}

void __stdcall Z_OnAfterCreateWindow(PEvent e) {

	for (int i = 0x004df7b8; i <= 0x004dfa31; i++)
		Zecondary_Skills->WriteByte(i, 0x90);

	replace_byte patch[] = {
		{0x006600f8, 'z'}, // zecsk82.def
		{0x006817d0, 'z'}, // zecsk32.def 
		{0x006601d0, 'z'}, // zecskill.def
		{0x00677280, 'z'}, // zSkilBon.def

		{0x0, 0x0}
	};
	
	for (int i = 0; patch[i].address; i++)
		Zecondary_Skills->WriteByte(patch[i].address, patch[i].new_byte);

	/**
	for (int i = 0; SS_text_dwords[i].address; i++)
		Zecondary_Skills->WriteDword(SS_text_dwords[i].address, SS_text_dwords[i].new_dword);
	*/
	//Zecondary_Skills->CreateHiHook(0x004DF7E0,SPLICE_,EXTENDED_,STDCALL_,hook_HERO_Screen_Preset);
	// Zecondary_Skills->WriteLoHook (0x004DF7d0, hook_HERO_Screen_Preset);
	Zecondary_Skills->WriteLoHook(0x00754C10, hook_HERO_Screen_Refresh);
	// Zecondary_Skills->WriteJmp(0x004df7f0, (int) asm_HERO_Screen_Preset);

	Zecondary_Skills->WriteJmp(0x004df7f8, (int) show_HERO_SS_icons);

	Zecondary_Skills->WriteJmp(0x004df800, (int)show_HERO_SS_icons_original);

	// Zecondary_Skills->WriteLoHook(0x004F5D22, hook_HERO_zecsk82a);
	// Zecondary_Skills->WriteLoHook(0x004F5DAE, hook_HERO_zecsk82b);


	for (int i = 0x004DE7E5; i < 0x004DE843; i++)
		Zecondary_Skills->WriteByte(i, 0x90);
	Zecondary_Skills->WriteJmp(0x004DE7E5, (int)calc_pre_msgbox);

	
	for (int i = 0x004F5D0C; i < 0x004F5DAE; i++)
		Zecondary_Skills->WriteByte(i, 0x90);
	Zecondary_Skills->WriteJmp(0x004F5D0C, (int)calc_in_msgbox);
	

}

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		// SS_text_clean();
		globalPatcher = GetPatcher();
		Zecondary_Skills = globalPatcher->CreateInstance((char*)PINSTANCE_MAIN);
		ConnectEra();

		// Z_Replace_SS_text_dwords();

		/*
		for (int i = 0; SS_text_dwords[i].address; i++)
			Zecondary_Skills->WriteDword(SS_text_dwords[i].address, SS_text_dwords[i].new_dword);
		*/

		////code here
		//RegisterHandler(InitializeHandler2, "OnAfterLoadGame");
		//RegisterHandler(InitializeHandler2, "OnAfterErmInstructions");
		//ApiHook((void*)Hook_Initialize1, HOOKTYPE_CALL, (void*)0x00402340);
		//ApiHook((void*)Hook_Initialize2, HOOKTYPE_JUMP, (void*)0x00402450);


		RegisterHandler(Z_OnAfterCreateWindow, (char*)"OnAfterCreateWindow");

		RegisterHandler(Z_OnAfterWog,  (char*)"OnAfterWoG");
		RegisterHandler(Z_OnGameEnter, (char*)"OnGameEnter");
		RegisterHandler(Z_OnPreHeroScreen, (char*)"OnPreHeroScreen");
		RegisterHandler(Z_OnBeforeHeroInteraction, (char*) "OnBeforeHeroInteraction");
		
		RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterLoadGame");
		RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterErmInstructions");
		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnBeforeWoG");
		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterWoG");

		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnAfterWoG");

		// RegisterHandler(Z_Replace_SS_text_dwords, (char*)"OnBeforeWoG");

		//ApiHook((void*)Hook_Blank, HOOKTYPE_CALL, (void*)0x00402340);
	}
	return TRUE;
};

