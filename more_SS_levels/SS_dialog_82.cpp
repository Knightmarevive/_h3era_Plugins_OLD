//#include"MyTypes.h"
typedef void(*voidfun)(void);
voidfun calc_pre_msgbox_ret = (voidfun)0x004DE843;
voidfun calc_in_msgbox_ret  = (voidfun)0x004F5DAE;
voidfun fun_41B2A0			= (voidfun)0x41B2A0;
voidfun fun_404A20			= (voidfun)0x00404A20;
voidfun fun_404A40			= (voidfun)0x00404A40;

extern char* Skill_Levels[];

void calc_pre_msgbox(void);

// patched from 0x004F5D0C
__declspec(naked) void calc_in_msgbox(void) {
	_asm {
		cmp dword ptr ds : [ebx + 4], 0x400
		jge new_dialog
		pushad
		mov eax, dword ptr ds : [ebx + 4]
		sub eax, 3
		mov ecx, 3 
		idiv ecx 
		shl eax, 4
		add eax, edx
		add eax, 0x10
		inc eax
		mov dword ptr ds : [ebx + 4], eax
		popad
		jmp z_begin
		new_dialog:
		sub dword ptr ds : [ebx+4], 0x400
		
		z_begin:
		cdq
		//mov ecx, 0x3
		push 0x1
		//idiv ecx
		or ecx, 0xFFFFFFFF
		// xor eax, eax

			mov eax, dword ptr ds : [ebx + 4]
			and eax, 0x0f

		// mov esi, dword ptr ds : [edx * 4 + Skill_Levels]
		mov esi, dword ptr ds : [eax * 4 + Skill_Levels]

			xor eax, eax
		lea edx, dword ptr ds : [ebx + 0x18]
		mov edi, esi
		repne scasb
		not ecx
		dec ecx
		mov dword ptr ss : [ebp + 0x8] , ecx
		push ecx
		mov ecx, edx
		call fun_404A40
		test al, al
		je label_4F5D5C
		mov ecx, dword ptr ss : [ebp + 0x8]
		mov edi, dword ptr ds : [ebx + 0x1C]
		lea eax, dword ptr ds : [ebx + 0x18]
		mov edx, ecx
		shr ecx, 0x2
		rep movsd
		mov ecx, edx
		and ecx, 0x3
		rep movsb
		mov ecx, edx
		push ecx
		mov ecx, eax
		call fun_404A20

		label_4F5D5C:
		mov edi, 0x660330
		or ecx, 0xFFFFFFFF
		xor eax, eax
		lea esi, dword ptr ds : [ebx + 0x18]
		repne scasb
		not ecx
		dec ecx
		push ecx
		push 0x660330
		mov ecx, esi
		call fun_41B2A0

		mov ecx, dword ptr ds : [ebx + 0x4]
		// mov eax, 0x55555556
		// imul ecx
		
		mov edx, ecx
		mov ecx, dword ptr ds : [0x0067DCF0]  // mov ecx, dword ptr ds : [<SecondarySkillInfo_ptr>]
		// mov eax, edx
		// shr eax, 0x1F
		// add edx, eax
		// xor eax, eax
		
		and edx, 0xfffffff0
		// shl edx, 0x6
		// mov edx, dword ptr ds : [ebx + 0x4]
		
		// mov edx, dword ptr ds : [edx + ecx - 0x40]
		mov edx, dword ptr ds : [ecx +edx*4 - 0x40]

		or ecx, 0xFFFFFFFF
		mov edi, edx
		repne scasb
		not ecx
		dec ecx
		push ecx
		push edx
		mov ecx, esi
		xor eax, eax
		call fun_41B2A0

		jmp calc_in_msgbox_ret
	}
}



// patched from 0x004DE7E5
__declspec(naked) void calc_pre_msgbox(void) {
	_asm {
		shl edx, 4
		lea eax, dword ptr ds : [ecx + edx]
		shr edx, 4

		mov ecx, dword ptr ds : [0x0067DCF0]
		push 0x0
		push 0xFFFFFFFF
		mov edi, dword ptr ds : [ecx + eax * 4]
		or ecx, 0xFFFFFFFF
		xor eax, eax
		push 0x0
		repne scasb
		not ecx
		sub edi, ecx
		push 0xFFFFFFFF
		mov eax, ecx
		mov esi, edi
		mov edi, 0x697428
		shr ecx, 0x2
		rep movsd
		mov ecx, eax
		mov eax, dword ptr ds : [0x698B70]
		and ecx, 0xf // 0x3
		rep movsb

		//lea ecx, dword ptr ds : [edx + edx * 2]
		//movsx edx, byte ptr ds : [eax + edx + 0xC9]
		//lea eax, dword ptr ds : [ecx + edx + 0x2]
		
		mov ecx, edx
		// dec ecx
		shl ecx, 4
		movsx edx, byte ptr ds : [eax + edx + 0xC9]
		lea eax, dword ptr ds : [ecx + edx + 0x410]


		push eax
		push 0x14
		neg bl
		sbb ebx, ebx
		push 0xFFFFFFFF
		and ebx, 0xf // 0x3
		push 0xFFFFFFFF
		inc ebx
		mov ecx, 0x697428
		mov edx, ebx
		
		jmp calc_pre_msgbox_ret
	}
}





//////////////// originals


// patched from 0x004F5D0C
__declspec(naked) void calc_in_msgbox_original(void) {
	_asm {
		cdq
		mov ecx, 0x3
		push 0x1
		idiv ecx
		or ecx, 0xFFFFFFFF
		xor eax, eax
		mov esi, dword ptr ds : [edx * 4 + Skill_Levels]
		lea edx, dword ptr ds : [ebx + 0x18]
		mov edi, esi
		repne scasb
		not ecx
		dec ecx
		mov dword ptr ss : [ebp + 0x8] , ecx
		push ecx
		mov ecx, edx
		call fun_404A40
		test al, al
		je label_4F5D5C
		mov ecx, dword ptr ss : [ebp + 0x8]
		mov edi, dword ptr ds : [ebx + 0x1C]
		lea eax, dword ptr ds : [ebx + 0x18]
		mov edx, ecx
		shr ecx, 0x2
		rep movsd
		mov ecx, edx
		and ecx, 0x3
		rep movsb
		mov ecx, edx
		push ecx
		mov ecx, eax
		call fun_404A20

		label_4F5D5C:
		mov edi, 0x660330
		or ecx, 0xFFFFFFFF
		xor eax, eax
		lea esi, dword ptr ds : [ebx + 0x18]
		repne scasb
		not ecx
		dec ecx
		push ecx
		push 0x660330
		mov ecx, esi
		call fun_41B2A0
		mov ecx, dword ptr ds : [ebx + 0x4]
		mov eax, 0x55555556
		imul ecx
		mov ecx, dword ptr ds : [0x0067DCF0]  // mov ecx, dword ptr ds : [<SecondarySkillInfo_ptr>]
		mov eax, edx
		shr eax, 0x1F
		add edx, eax
		xor eax, eax
		shl edx, 0x4
		mov edx, dword ptr ds : [edx + ecx - 0x10]
		or ecx, 0xFFFFFFFF
		mov edi, edx
		repne scasb
		not ecx
		dec ecx
		push ecx
		push edx
		mov ecx, esi
		call fun_41B2A0

		jmp calc_in_msgbox_ret
	}
}

// patched from 0x004DE7E5
__declspec(naked) void calc_pre_msgbox_original(void) {
	_asm {
		lea eax, dword ptr ds : [ecx + edx * 4]
		mov ecx, dword ptr ds : [0x0067DCF0]
		push 0x0
		push 0xFFFFFFFF
		mov edi, dword ptr ds : [ecx + eax * 4]
		or ecx, 0xFFFFFFFF
		xor eax, eax
		push 0x0
		repne scasb
		not ecx
		sub edi, ecx
		push 0xFFFFFFFF
		mov eax, ecx
		mov esi, edi
		mov edi, 0x697428
		shr ecx, 0x2
		rep movsd
		mov ecx, eax
		mov eax, dword ptr ds : [0x698B70]
		and ecx, 0x3
		rep movsb
		lea ecx, dword ptr ds : [edx + edx * 2]
		movsx edx, byte ptr ds : [eax + edx + 0xC9]
		lea eax, dword ptr ds : [ecx + edx + 0x2]
		push eax
		push 0x14
		neg bl
		sbb ebx, ebx
		push 0xFFFFFFFF
		and ebx, 0x3
		push 0xFFFFFFFF
		inc ebx
		mov ecx, 0x697428
		mov edx, ebx
		
		jmp calc_pre_msgbox_ret
	}
}