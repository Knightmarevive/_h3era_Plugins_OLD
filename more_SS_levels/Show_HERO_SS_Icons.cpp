typedef void(*voidfun)(void);

voidfun fun_5FE2D0 = (voidfun) 0x5FE2D0;
voidfun fun_617492 = (voidfun) 0x617492;
voidfun fun_4EA800 = (voidfun) 0x4EA800;
voidfun ret_4df800 = (voidfun) 0x004df800;
voidfun ret_4dfa36 = (voidfun) 0x004dfa36;
char* Zecskill_def = (char*)0x006601d0;
const char* void_def = "void.def";

void HERO_Screen_Calc(void);
extern int show_hero_SS[10];

//char* show_hero_SS_address = (char*) show_hero_SS;

///// modified code
__declspec(naked) void show_HERO_SS_icons(void) {
	_asm {

		//call show_HERO_SS_icons_original
		//lea ecx, dword ptr ss : [ebp - 0x20] // missing line

		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
		 
		nop
		nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2B
		// je label_4DF7FF
 
		//mov ebx, 1
		//call HERO_Screen_Calc
		// mov ebx, [show_hero_SS+1*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x44F
		push 0x2C
		push 0x2C
		push 0x114
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF801

			label_4DF7FF:
		xor eax, eax

			label_4DF801:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			nop 
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2C
		// je label_4DF851

			 
			nop


			// mov ebx, [show_hero_SS+2*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x450
		push 0x2C
		push 0x2C
		push 0x114
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF853

			label_4DF851:
		xor eax, eax

			label_4DF853:
		lea ecx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2D
		// je label_4DF8A0

			
			nop

			// mov ebx, [show_hero_SS+3*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x451
		push 0x2C
		push 0x2C
		push 0x144
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF8A2

			label_4DF8A0:
		xor eax, eax

			label_4DF8A2:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2E
		// je label_4DF8F2

			nop

			// mov ebx, [show_hero_SS+4*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x452
		push 0x2C
		push 0x2C
		push 0x144
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF8F4

			label_4DF8F2:
		xor eax, eax

			label_4DF8F4:
		lea ecx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			nop
			

			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x2F
		// je label_4DF941
		
			nop

			// mov ebx, [show_hero_SS+5*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x453
		push 0x2C
		push 0x2C
		push 0x174
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF943

			label_4DF941:
		xor eax, eax

			label_4DF943:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x30
		// je label_4DF993

			
			nop

			// mov ebx, [show_hero_SS+6*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x454
		push 0x2C
		push 0x2C
		push 0x174
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF995

			label_4DF993:
		xor eax, eax

			label_4DF995:
		lea ecx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
		
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x31
		// je label_4DF9E2
		

			// mov ebx, [show_hero_SS+7*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x455
		push 0x2C
		push 0x2C
		push 0x1A4
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF9E4

			label_4DF9E2:
		xor eax, eax

			label_4DF9E4:
		lea edx, dword ptr ss : [ebp - 0x20]
		mov byte ptr ss : [ebp - 0x4] , bl
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push edx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
			 
			nop
			nop
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, 0
		mov byte ptr ss : [ebp - 0x4] , 0x32
		// je label_4DFA34

			
			nop
			nop

			// mov ebx, [show_hero_SS+8*4]

		push 0x10
		push 0
		push 0
		push 0
		push ebx
		push Zecskill_def
		push 0x456
		push 0x2C
		push 0x2C
		push 0x1A4
		push 0xA1
		mov ecx, eax
		call fun_4EA800
		jmp label_4dfa36

			label_4DFA34:
		xor eax, eax

			label_4dfa36:
		nop

			mov ebx, 0;

		nop

		jmp ret_4df800
	}
}

#define Zecskill_def void_def

///// original code
__declspec(naked) void show_HERO_SS_icons_original(void) {
	_asm {

		lea ecx, dword ptr ss : [ebp - 0x20] // missing line
		
		mov dword ptr ss : [ebp - 0x20] , eax
		mov eax, dword ptr ds : [esi + 0x8]
		push ecx
		push 0x1
		push eax
		mov ecx, esi
		call fun_5FE2D0
		push 0x48
		call fun_617492
		add esp, 0x4
		mov dword ptr ss : [ebp - 0x1C] , eax
		cmp eax, ebx
		mov byte ptr ss : [ebp - 0x4] , 0x2B
		je label_4DF7FF
		push 0x10
		push ebx
		push ebx
		push ebx
		push ebx
		push Zecskill_def
		push 0x4F
		push 0x2C
		push 0x2C
		push 0x114
		push 0x12
		mov ecx, eax
		call fun_4EA800
		jmp label_4DF801

		label_4DF7FF :
		xor eax, eax

			label_4DF801 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2C
			je label_4DF851
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x50
			push 0x2C
			push 0x2C
			push 0x114
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF853

			label_4DF851 :
		xor eax, eax

			label_4DF853 :
		lea ecx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push ecx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2D
			je label_4DF8A0
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x51
			push 0x2C
			push 0x2C
			push 0x144
			push 0x12
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF8A2

			label_4DF8A0 :
		xor eax, eax

			label_4DF8A2 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2E
			je label_4DF8F2
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x52
			push 0x2C
			push 0x2C
			push 0x144
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF8F4

			label_4DF8F2 :
		xor eax, eax

			label_4DF8F4 :
		lea ecx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push ecx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x2F
			je label_4DF941
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x53
			push 0x2C
			push 0x2C
			push 0x174
			push 0x12
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF943

			label_4DF941 :
		xor eax, eax

			label_4DF943 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x30
			je label_4DF993
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x54
			push 0x2C
			push 0x2C
			push 0x174
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF995

			label_4DF993 :
		xor eax, eax

			label_4DF995 :
		lea ecx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push ecx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x31
			je label_4DF9E2
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x55
			push 0x2C
			push 0x2C
			push 0x1A4
			push 0x12
			mov ecx, eax
			call fun_4EA800
			jmp label_4DF9E4

			label_4DF9E2 :
		xor eax, eax

			label_4DF9E4 :
		lea edx, dword ptr ss : [ebp - 0x20]
			mov byte ptr ss : [ebp - 0x4] , bl
			mov dword ptr ss : [ebp - 0x20] , eax
			mov eax, dword ptr ds : [esi + 0x8]
			push edx
			push 0x1
			push eax
			mov ecx, esi
			call fun_5FE2D0
			push 0x48
			call fun_617492
			add esp, 0x4
			mov dword ptr ss : [ebp - 0x1C] , eax
			cmp eax, ebx
			mov byte ptr ss : [ebp - 0x4] , 0x32
			je label_4DFA34
			push 0x10
			push ebx
			push ebx
			push ebx
			push ebx
			push Zecskill_def
			push 0x56
			push 0x2C
			push 0x2C
			push 0x1A4
			push 0xA1
			mov ecx, eax
			call fun_4EA800
			jmp label_4dfa36

			label_4DFA34 :
		xor eax, eax

			label_4dfa36 :

		//last line is modified

		jmp ret_4dfa36
	}
}