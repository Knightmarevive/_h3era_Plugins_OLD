//#include <windows.h>
//#include "era.h"
//#include "heroes.h"

//using namespace Era;


//DWORD PlaceNPC2_addr_call = 0x76c06b;

//DWORD PlaceNPC2_begin = 0x76c10d;

//DWORD PlaceNPC2_return = 0x76c33a;


//DWORD PlaceNPC2_begin = 0x76AF17;
DWORD PlaceNPC2_what_is_patched = 0x76b804;
DWORD PlaceNPC2_what_is_patched_return = 0x76ba49;

DWORD PlaceNPC2_middle_1 = 0x0076BA3E;
DWORD PlaceNPC2_middle_2 = 0x0076BA43;

BOOL __stdcall __PlaceNPC2__HOOK3(THookContext* Context) {
	Context->RetAddr = PlaceNPC2_what_is_patched_return;
	return !EXEC_DEF_CODE;
}



extern Byte *PutStack(Byte *Bm, int Type, int Num, int Pos, int Side, HERO *hp, int Placed, int Stack);

bool isMonsterTent(int Type) {
	if (Type == 147 || Type == 303) return true;
	if (Type >  305 && Type <  315) return true;
	if (Type >  316 && Type <  325) return true;
	return false;
}

bool isMonsterBallista(int Type) {

	if (Type == 146 || Type == 316 || Type == 305) return true;
	return false;
}

Byte *PlaceNPC2(Byte *Bm, int Type, int Side, BYTE *npc, HERO *Hp, int *Placed)
{
//#include "templ.h"
	int   i, pos, num, tp, placed;
	Byte *mstr, *mon;
	
	//majaczek
	DWORD npc_Type = *((DWORD*)(Byte*)(npc + 0x0c));
	DWORD npc_Level = *((DWORD*)(Byte*)(npc + 0x011c));
	//DWORD npc_Type = 0; DWORD npc_Level = 7;

	if (Side == 0) pos = 88; else pos = 98;
	mstr = PutStack(Bm, Type, 1, pos, Side, Hp, *Placed, -1); ++*Placed;
	
	if (npc_Type == 1) { // ������� �������� � 0x46344A
		num = npc_Level/2 + 2;
		if (Side == 0) pos = 153; else pos = 169;
		placed = 0;
		for (i = 0; i<21; i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)]; tp = *(int *)&mon[0x34];
			if (/*tp == 147*/ isMonsterTent(tp)) { *(int *)&mon[0x60] += (num +2); *(int *)&mon[0x4C] += (num +2); placed = 1; break; }
		}
		if (placed == 0) {
			if (Side == 0) pos = 153; else pos = 169; // 3.57m2

			int attacker[] = { 313,308,307,306,303,309,310,311,312,147,147,147,147,147,147 };
			int defender[] = { 324,319,318,317,314,320,321,322,323,147,147,147,147,147,147 };
			int *current = Side ? defender : attacker;

			PutStack(Bm, /*147*/ current[npc_Type], num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}
	}

	if (npc_Type == 2) { // ������� �������� � 0x46344A
		num = (npc_Level+1) / 3 + 2;
		if (Side == 0) pos = 17; else pos = 33;
		placed = 0;
		for (i = 0; i<21; i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)]; tp = *(int *)&mon[0x34];
			if (tp == 148) { *(int *)&mon[0x60] += (num + 2); *(int *)&mon[0x4C] += (num + 2); placed = 1; break; }
		}
		if (placed == 0) {
			if (Side == 0) pos = 17; else pos = 33; // 3.57m2
			PutStack(Bm, 148, num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}
	}

	if (npc_Type == 6) { // ������� �������� � 0x4633E8
		num = (npc_Level + 1) / 6 + 2;
		if (Side == 0) pos = 51; else pos = 67;
		placed = 0;
		for (i = 0; i<21; i++) {
			mon = &Bm[0x54CC + 0x548 * (i + Side * 21)]; tp = *(int *)&mon[0x34];
			if (/* tp == 146*/ isMonsterBallista(tp) ) { *(int *)&mon[0x60] += (num +2); *(int *)&mon[0x4C] += (num +2); placed = 1; break; }
		}
		if (placed == 0) {
			if (Side == 0) pos = 51; else pos = 67; // 3.57m2

			int attacker[] = { 305,305,305,305,305,305,305,305,305,146,146,146,146,146,146 };
			int defender[] = { 316,316,316,316,316,316,316,316,316,146,146,146,146,146,146 };
			int *current = Side ? defender : attacker;

			PutStack(Bm, /*146*/ current[npc_Type] , num, pos, Side, Hp, *Placed, -1); ++*Placed;
		}
	}
	
	//RETURN(mstr)
	//__asm pushad
	//__asm call CS:0x007712b0
	//__asm popad
	return(mstr);
}



BOOL __stdcall __PlaceNPC2__HOOK2(THookContext* Context) {
	//DWORD temp1, Temp2, Temp3; Temp2 = Context->ESP; DWORD ADDR, temp4;
	Byte *Bm; int zType; int Side; BYTE *npc; HERO *Hp; int *Placed;
	
	DWORD* ARGS = (DWORD*)(Byte*)(Context->EBP + 8);
	Bm = (Byte*) ARGS[0]; 
	zType = ARGS[1]; Side = ARGS[2]; 
	npc = (Byte*) ARGS[3];
	Hp = (HERO *) ARGS[4];
	Placed = (int*) ARGS[5];
	Context->EAX = (DWORD) PlaceNPC2(Bm,zType,Side,npc, Hp, Placed);
	//Context->RetAddr = PlaceNPC2_what_is_patched_return;
	Context->RetAddr = PlaceNPC2_middle_1;
	return !EXEC_DEF_CODE;

}

/*
void __stdcall _OnAfterCreateWindow_2_(TEvent* Event)
{
	ApiHook((void*)__PlaceNPC2__,
		HOOKTYPE_BRIDGE, (void*)PlaceNPC2_what_is_patched);
}

*/