#include "..\__include\era.h"

extern "C" {
	void  __fastcall  UndefinedFunction_00402450(  int /*,  int */ );

	/*
	int UndefinedFunction_00402450_Caller_asm_helper_EBX;
	int UndefinedFunction_00402450_Caller_asm_helper_EDX;
	int UndefinedFunction_00402450_Caller_asm_helper_EBP;
	int UndefinedFunction_00402450_Caller_asm_helper_ESP;
	__declspec(naked) void UndefinedFunction_00402450_Caller(void) {
		__asm {
			mov UndefinedFunction_00402450_Caller_asm_helper_EDX, EDX;
			mov UndefinedFunction_00402450_Caller_asm_helper_EBX, EBX;
			mov UndefinedFunction_00402450_Caller_asm_helper_ESP, ESP;
			mov UndefinedFunction_00402450_Caller_asm_helper_EBP, EBP;

			mov edx, eax; 
			mov edx, ecx;
			pushad;
			push edx;
			push ecx;
			call UndefinedFunction_00402450;
			popad;

			// mov ESP, UndefinedFunction_00402450_Caller_asm_helper_ESP;
			// push ebx;
			// mov ESP, UndefinedFunction_00402450_Caller_asm_helper_ESP;
			mov EDX, UndefinedFunction_00402450_Caller_asm_helper_EDX;
			mov EBX, UndefinedFunction_00402450_Caller_asm_helper_EBX;
			mov EBP, UndefinedFunction_00402450_Caller_asm_helper_EBP;
			mov ESP, UndefinedFunction_00402450_Caller_asm_helper_ESP;
			retn;
		}
	}
	*/
}


using namespace Era;

/*
void __stdcall InitializeHandler1(TEvent* Event) {
	long  patched_adress = 0x00402341; // 4203329
	//char* patched_ptr_c = (char*)patched_adress;
	//int * patched_ptr_i = (int*)patched_ptr_c;
	int * patched_ptr_i = (int*)patched_adress;
	*patched_ptr_i = (int) (void*) UndefinedFunction_00402450;
}
*/

void __stdcall InitializeHandler2(TEvent* Event) {

	int  patched_adress = 0x00402341; // 4203329
	//char* patched_ptr_c = (char*)patched_adress;
	//int * patched_ptr_i = (int*)patched_ptr_c;
	int * patched_ptr_i = (int*)patched_adress;
	*patched_ptr_i = ((int)(void*)UndefinedFunction_00402450 /*_Caller*/) - patched_adress -4;
}

/*
BOOL __stdcall Hook_Initialize1(THookContext* Context) {
	UndefinedFunction_00402450(Context->ECX, Context->EDX );
	return !EXEC_DEF_CODE;
}


BOOL __stdcall Hook_Blank(THookContext* Context) {
	return !EXEC_DEF_CODE;
}
*/

/*
BOOL __stdcall Hook_Initialize2(THookContext* Context) {
	UndefinedFunction_00402450( Context->ECX);
	return !EXEC_DEF_CODE;
}
*/

extern "C" __declspec(dllexport) BOOL APIENTRY DllMain(HINSTANCE hInst, DWORD reason, LPVOID lpReserved)
{
	if (reason == DLL_PROCESS_ATTACH)
	{
		ConnectEra();
		
		////code here
		RegisterHandler(InitializeHandler2, "OnAfterLoadGame");
		RegisterHandler(InitializeHandler2, "OnAfterErmInstructions");
		//ApiHook((void*)Hook_Initialize1, HOOKTYPE_CALL, (void*)0x00402340);
		//ApiHook((void*)Hook_Initialize2, HOOKTYPE_JUMP, (void*)0x00402450);


		//ApiHook((void*)Hook_Blank, HOOKTYPE_CALL, (void*)0x00402340);
	}
	return TRUE;
};
