#pragma once
extern "C" {
__declspec(align(8)) struct cheattext{
char s_ajpgevavgl_0063a480 [200]= "jbttnynqevry";//  "ajpgevavgl"  ;   // good army
char s_ajpntragf_0063a48c[200] = "jbtcngubsgurqrnq";//  "ajpntragf"  ;    // evil army
char s_ajpybgfbsthaf_0063a498[200] = "jbtbyvcunhag";//  "ajpybgfbsthaf"  ; // war machines
char s_ajparb_0063a4a8[200] = "jbttnaqnysjuvgr";// "ajparb" ; //levelup
char s_ajpsbyybjgurjuvgrenoovg_0063a4b0[200] = "jbtovyob";// "ajpsbyybjgurjuvgrenoovg"  ; //lucky
char s_ajparohpunqarmmne_0063a4c8[200] = "jbtfunqbjsnk";// "ajparohpunqarmmne"  ;  //tireless
char s_ajpzbecurhf_0063a4dc[200] = "jbtsryybjfuvc";//  "ajpzbecurhf"  ;  //brave
char s_ajpbenpyr_0063a4e8[200] = "jbtcnynagve";// "ajpbenpyr"  ;   // puzzle map
char s_ajpjungvfgurzngevk_0063a4f4[200] = "jbtrlrbsfnheba";// "ajpjungvfgurzngevk"; //iseeeverything
char s_ajpvtabenaprvfoyvff_0063a508[200] = "jbtzbeqbe";// "ajpvtabenaprvfoyvff"; //iamblind
char s_ajpgurpbafgehpg_0063a51c[200] = "jbtvfratneq"; // "ajpgurpbafgehpg"  ; //iamrich
char s_ajpoyhrcvyy_0063a52c[200] = "jbtqnexybeq";// "ajpoyhrcvyy"  ; //iamloser
char s_ajperqcvyy_0063a538[200] = "jbtbarevat";// "ajperqcvyy"  ; //wearethechampions
char s_ajpgurervfabfcbba_0063a544[200] = "jbtfnehzna"; //"ajpgurervfabfcbba"  ; //idomagic
char s_ajpmvba_0063a558[200] = "jbtzvanfgvevgu";// "ajpmvba"; //instant builder
char s_ajpcuvfurecevpr_0063a560[200] = "jbtfnehznabsznalpbybef";//"ajpcuvfurecevpr"; //colorify - disabled
////////////////////////////////////////
char s_ajpajpajp[200] = "ajpajpajp"; // debug - disabled
char s_cheater[200] = "vnzgurpurngrenaqgurtnzrvfzlgurngre";
char s_msg[200] = "";
char s_jbtvyhingne[200] = "jbtvyhingne"; // enable ERM codes
char s_jbtjubnzv[200] = "jbtjubnzv"; // hero info box
char s_jbtzbetbgu[200] = "jbtzbetbgu";// disable ERM codes
char s_jbtznvne[200] = "jbtznvne"; // give level 8 creatures
char s_jbtinyne[200] = "jbtinyne"; // give Town Guardian creatures
char s_jbtyrtbynf[200] = "jbtyrtbynf"; // set all SS to Expert
char s_jbttvzyv[200] = "jbttvzyv"; // set all PS to 77
};
cheattext thecheattext;
}

extern "C" {
	int    DAT_0069ccfc = 0x0069ccfc;
	int    DAT_00699538 = 0x00699538;
	int    DAT_0063a608 = 0x0063a608;
	void(__thiscall  *FUN_00402a30)(int iParm1, undefined1 *param_1) = (void(__thiscall *)(int, undefined1 *))0x00402a30;// char* FUN_00402a30;
	uint(*FUN_006197c0)(char *param_1, char *param_2) = (uint(__cdecl *)(char *, char *))  0x006197c0 /* 0x0070692 */;
	undefined4(__thiscall *FUN_0044a9b0)(int _/*int *piParm1 */, int param_1, int param_2, int param_3) = (undefined4(__thiscall *)(int, int, int, int)) 0x0044a9b0;
	void(__thiscall *FUN_00415d40)(int iParm1, undefined4 param_1, char param_2, char param_3) = (void(__thiscall *)(int, undefined4, char, char)) 0x00415d40;//char* FUN_00415d40;
	undefined4(__thiscall *FUN_004e32e0)(int iParm1, int *param_1, undefined4 param_2, char param_3) = (undefined4(__thiscall *)(int, int *, undefined4, char)) 0x004e32e0;// char* FUN_004e32e0;
	int(__fastcall *FUN_004da690)(int iParm1) = (int(__fastcall *)(int)) 0x004da690;//long(*FUN_004da690) (void);
	int(__thiscall *FUN_004e3620)(int iParm1, int param_1, int param_2, char param_3) = (int(__thiscall *)(int, int, int, char)) 0x004e3620;// char* FUN_004e3620;
	/* void */ int(__fastcall *FUN_004e5000)(int iParm1) = (int(__fastcall *)(int)) 0x004e5000;// long(*FUN_004e5000) (void);
	void(__fastcall *FUN_0041a750)(int iParm1) = (void(__fastcall *)(int)) 0x0041a750;//void (* FUN_0041a750)(void);
	int DAT_0069ccf4 = 0x0069ccf4;  //code** DAT_0069ccf4 = (code**)0x0069ccf4;
	void(__thiscall *FUN_0049cdd0)(int iParm1, int param_1, int param_2, uint param_3,  code **param_4 /* char* param_4 */, int param_5, code **param_6) = (void(__thiscall *)(int, int, int, uint, code** /* char * */, int, code **)) 0x0049cdd0;//char* FUN_0049cdd0;
	void(__thiscall *FUN_00559170)(int iParm1, char param_1, undefined4 param_2) = (void(__thiscall *)(int, char, undefined4)) 0x00559170;//char* FUN_00559170;
	void(__fastcall *FUN_004f3370)(int iParm1) = (void(__fastcall *)(int)) 0x004f3370;// void(*FUN_004f3370) (void);
	void(__thiscall *FUN_004d95a0)(int iParm1, int param_1) = (void(__thiscall *)(int, int)) 0x004d95a0;// char* FUN_004d95a0;
	int DAT_0069e600 = 0x0069e600;
	void(*FUN_0055a070) (void) = (void(__cdecl *)(void)) 0x0055a070;
	//void(__thiscall *FUN_00417380)(int iParm1, char param_1) = (void(__thiscall *)(int, char)) 0x00417380;// char* FUN_00417380;
	void(*FUN_0055a260) (void) = (void(__cdecl *)(void)) 0x0055a260;
	int  DAT_006aaac4 =  0x006aaac4;
	void(__thiscall *FUN_0049d040)(int iParm1, int param_1, int param_2, uint param_3, int param_4, /* code **param_5 */ int param_5) = (void(__thiscall *)(int, int, int, uint, int, int)) 0x0049d040;//char* FUN_0049d040;
	//void(*FUN_004c7910) (void) = (void(__cdecl *)(void)) 0x004c7910;
	void(__fastcall *FUN_0040ebe0)(int _, int iParm1) = (void(__fastcall *)(int, int)) 0x0040ebe0;// char* FUN_0040ebe0;
	int DAT_006a5dc4 = 0x006a5dc4;
	void(*FUN_0060b0fb) (void) = (void(__cdecl *)(void)) 0x0060b0fb;
	void(__thiscall *FUN_00404130)(int iParm1, char param_1) = (void(__thiscall *)(int, char)) 0x00404130;//char* FUN_00404130;
	void(__thiscall *FUN_00404b80)(int iParm1, uint param_1) = (void(__thiscall *)(int, uint)) 0x00404b80;// char* FUN_00404b80;
	int DAT_0069779c = 0x0069779c;
	//////////////////////////////////////////////////////////////////////////////////
	int DAT_006992b8 = 0x006992b8;
	void(__fastcall *FUN_004c7910) (int,int) = (void(__fastcall *)(int,int)) 0x004c7910;
	void(__thiscall *FUN_00417380)(int iParm1, char, char) = (void(__thiscall *)(int, char, char)) 0x00417380;// char* FUN_00417380;

}

namespace Era {

	typedef char  TErmZVar[512];
	extern TErmZVar* z;
	extern int* v;
	typedef void(__stdcall *TExecErmCmd) (char* CmdStr);
	extern TExecErmCmd ExecErmCmd;
}