
#include "emerald.h"
//extern GAMEDATA2 no_save;

/*
extern int enchanted_artifacts[];
extern int enchanted_artifacts_count;
*/

void ParseArray(char* buf, char* name, int* cortage)
{

}

void ParseInt(char* buf, char* name, int* result)
{ 
	char *c = strstr(buf,name);
	if(c!=NULL)
		*result = atoi(c+strlen(name));
}

void ParseByte(char* buf, char* name, char* result)
{ 
	char *c = strstr(buf,name);
	if(c!=NULL)
		*result = (char)atoi(c+strlen(name));
}


bool ParseStr(char* buf, char* name, /*char* &target */ char res[char_table_size])
{
	// char res[char_table_size];
	char *c = strstr(buf,name);
	int l = strlen(name);
	if (c != NULL)
	{
		char* cend = strstr(c + l, "\"");
		if (cend != NULL) //���� ��, ����� ������ �����������
		{
			//*res = (char*)malloc(cend-c+1);

			//memset(*res,0,cend-c+1);
			memset(res, 0, cend - c + 1);

			for (int i = 0, j = 0; j < (cend - c) - l; i++, j++)
			{
				if (c[l + j] != '\\')
				{
					//(*res)[i] = c[l+j];
					res[i] = c[l + j];
				}
				else
				{
					if (c[l + j + 1] == 'r')
					{
						//(*res)[i] = '\r';
						res[i] = '\r';

						j++;
						continue;
					}
					else if (c[l + j + 1] == 'n')
					{
						//(*res)[i] = '\n';
						res[i] = '\n';
						j++;
						continue;
					}
					else
					{
						//(*res)[i] = c[l+j];
						res[i] = c[l + j];

					}
				}
			}

			//strncpy(*res,c+l,cend-1-c);

			//(*res)[cend-c-l] = 0;

			res[cend - c - l] = 0;

			//res[cend - c - l] = 13; //end of WoG erm
			//res[cend - c - l +1] = 0;

			//sprintf(*res,*res);

			/*
			if (strcmp(target, res) != 0 ) {
				strcpy(target, res);
			}
			*/
		}
		return true;
	}
	else return false;
}
void ParseStr2(char* buf, char* name, char*& target_1, char*& target_2
	/* char res[char_table_size]*/, char* target_static, int& target_int ) {
	
	char res[char_table_size];
	if (ParseStr(buf, name, res)) {
		target_1 = target_2 = target_static;
		strcpy(target_static, res);
		target_int = 0;
	}

	/*
	if (target && target!= target2 && !*target)
		strcpy(target2, target);
	ParseStr(buf, name, target2);
	target = target2;
	target3 = 0;
	*/

	/*
	char res[char_table_size];
	ParseStr(buf, name, res);
	if (!target || strcmp(target, res) != 0) {
		//char* tmp = new char[char_table_size];
		//target = tmp;
		target = target2;
		strcpy(target, res);
	}
	*/
}


int ParseTuple(char* buf, char* name, /* int** tuple */ int tuple [int_tuple_size])
{
	  int len=0;
	  //char *tmp = (char*)malloc(strlen(buf));
	  static char tmp[32768];
	  char *c = strstr(buf,name);
	  if(c!= NULL) 
	  {
		  strncpy(tmp,c+strlen(name),int_tuple_size);
		  c = strpbrk(tmp,"\r\n\0}");
		  if(c!= NULL) 
		  {
			  tmp[c-tmp]=0;

			  char *p = strtok(tmp, ",");
			  
			  do 
			  {
				if(p) 
				{
					len++;
					// *tuple = (int*)realloc(*tuple,len*sizeof(int));
					// (*tuple)[len-1]=atoi(p);
					tuple[len - 1] = atoi(p);
				}
				p = strtok(NULL, ", ");
			  } while(p);
		  }
	  }
	//free(tmp);
	return len;
}

extern "C" __declspec(dllexport) void ConfigureArt(int artifact, char* config) {

	char* buf = config; int target = artifact;
	int tuple[int_tuple_size]; int len = 0;

	ParseInt(buf, "Cost=", &(EmeraldArtNewTable[target].cost));
	ParseInt(buf, "Rank=", &(EmeraldArtNewTable[target].rank));
	ParseInt(buf, "SlotID=", &(EmeraldArtNewTable[target].slot));
	ParseInt(buf, "ComboID=", &(EmeraldArtNewTable[target].combonum));
	ParseInt(buf, "ComboPart=", &(EmeraldArtNewTable[target].partof));

	ParseByte(buf, "Attack=", &(EmeraldArtNewBTable[target].att));
	ParseByte(buf, "Defence=", &(EmeraldArtNewBTable[target].def));
	ParseByte(buf, "Knowledge=", &(EmeraldArtNewBTable[target].knw));
	ParseByte(buf, "Spellpower=", &(EmeraldArtNewBTable[target].spp));

	ParseInt(buf, "Luck=", &no_save.luck_bonuses[target]);
	ParseInt(buf, "Morale=", &no_save.morale_bonuses[target]);
	ParseInt(buf, "LuckBP=", &no_save.luck_bonuses_bp[target]);
	ParseInt(buf, "MoraleBP=", &no_save.morale_bonuses_bp[target]);

	ParseInt(buf, "Fly=", &no_save.allow_fly[target]);
	ParseInt(buf, "Waterwalk=", &no_save.allow_water[target]);

	ParseInt(buf, "Autospell=", &no_save.autocast[target]);

	//ParseStr(buf, "Name=\"", &(newtable[target].name));
	ParseStr2(buf, "Name=\"", (EmeraldArtNewTable[target].name), 
		no_save.ArtSetUpBack[artifact].name,
		no_save.artNameTable[artifact],no_save.ArtNames[artifact].NameVar);

	//ParseStr(buf, "Description=\"", &(newtable[target].desc));
	ParseStr2(buf, "Description=\"", (EmeraldArtNewTable[target].desc), 
		no_save.ArtSetUpBack[artifact].desc,
		no_save.artDescTable[artifact], no_save.ArtNames[artifact].DescVar);

	//ParseStr(buf, "MapDesc=\"", &(arteventtable[target+1]));
	ParseStr(buf, "MapDesc=\"", (no_save.arteventtable[target + 1]));

	ParseStr(buf, "OnFirstAction=\"", (no_save.erm_on_battlestart[target]));
	ParseStr(buf, "OnCreatureProp=\"", (no_save.erm_on_creature_param_init[target]));

	ParseStr(buf, "OnUnequip=\"", (no_save.erm_on_ae0[target]));
	ParseStr(buf, "OnEquip=\"", (no_save.erm_on_ae1[target]));
	ParseStr(buf, "OnBattleStart=\"", (no_save.erm_on_ba52[target]));
	ParseStr(buf, "OnBattleEnd=\"", (no_save.erm_on_ba53[target]));
	ParseStr(buf, "OnEveryDay=\"", (no_save.erm_on_timer[target]));


	ParseInt(buf, "AttackCR=", &no_save.crattack_bonuses[target]);
	ParseInt(buf, "DefenceCR=", &no_save.crdefence_bonuses[target]);

	ParseInt(buf, "DamageMin=", &no_save.dmgmin_bonuses[target]);
	ParseInt(buf, "DamageMax=", &no_save.dmgmax_bonuses[target]);

	ParseInt(buf, "Speed=", &no_save.speed_bonuses[target]);
	ParseInt(buf, "Health=", &no_save.hp_bonuses[target]);

	ParseInt(buf, "Shots=", &no_save.shots_bonuses[target]);
	ParseInt(buf, "Casts=", &no_save.casts_bonuses[target]);



	len = ParseTuple(buf, "Immunities=", tuple);
	for (int i = 0; i != len; i++)
		no_save.spell_immunity[target][tuple[i]] = 1;

	int spell = -1;
	ParseInt(buf, "GivesSpell=", &spell);
	if (spell != -1)
	{
		spell ^= 0x80;
		//realloc(enchanted_artifacts,(enchanted_artifacts_count+1)*4);
		no_save.enchanted_artifacts[no_save.enchanted_artifacts_count + 1] = target;
		no_save.enchanted_artifacts_count++;

		EmeraldArtNewTable[target].spellflag = 1;
		no_save.artspelltable[target] = spell;
	}


}

 void LoadArtifactConfig(int target)
{
		//char *buf, fname[256];
		static char buf[65535];
		static char fname[1024];

		//int* tuple = NULL;
		// int tuple[int_tuple_size];

		// int len = 0;

        FILE *fdesc;

		//sprintf(fname, "data\\artifacts\\%u.cfg",target);
		/* sprintf(fname, "Mods\\Amethyst Upgrades\\Data\\artifacts\\%u.cfg", target);
		if (!FileExists(fname)) */
			sprintf(fname, "Mods\\Knightmare Kingdoms\\Data\\artifacts\\%u.cfg", target);
		if (!FileExists(fname))
			sprintf(fname, "Data\\artifacts\\%u.cfg", target);
		if (!FileExists(fname))
			MessageBoxA(0, fname, "File not found: ", 0);

        if(fdesc=fopen(fname,"r"))
        {

            
			//----------
            fseek (fdesc , 0 , SEEK_END);
            int fdesc_size=ftell(fdesc);
            rewind(fdesc);
            //----------
            //buf=(char*)malloc(fdesc_size+1);
			memset(buf,0,fdesc_size+1);
            fread(buf,1,fdesc_size,fdesc);
            //buf[fdesc_size]=0;
            fclose(fdesc);

			ConfigureArt(target, buf);

            //free(buf);
        }
}

 /*
void __stdcall blank(int first)
{
		memset(no_save.luck_bonuses + first * sizeof(int), 0, sizeof(no_save.luck_bonuses) - first * sizeof(int));
		memset(no_save.luck_bonuses_bp + first * sizeof(int), 0, sizeof(no_save.luck_bonuses_bp) - first * sizeof(int));
		memset(no_save.morale_bonuses + first * sizeof(int), 0, sizeof(no_save.morale_bonuses) - first * sizeof(int));
		memset(no_save.morale_bonuses_bp + first * sizeof(int), 0, sizeof(no_save.morale_bonuses_bp) - first * sizeof(int));
		memset(no_save.spell_immunity + first * sizeof(int), 0, sizeof(no_save.spell_immunity) - first * sizeof(int));
		memset(no_save.allow_fly + first * sizeof(int), 0, sizeof(no_save.allow_fly) - first * sizeof(int) );
		memset(no_save.allow_water + first * sizeof(int), 0, sizeof(no_save.allow_water) - first * sizeof(int) );
		memset(no_save.autocast + first * sizeof(int), 0, sizeof(no_save.autocast) - first * sizeof(int) );
		memset(no_save.crattack_bonuses + first * sizeof(int), 0, sizeof(no_save.crattack_bonuses) - first * sizeof(int) );
		memset(no_save.crdefence_bonuses + first * sizeof(int), 0, sizeof(no_save.crdefence_bonuses) - first * sizeof(int) );
		memset(no_save.dmgmin_bonuses + first * sizeof(int), 0, sizeof(no_save.dmgmin_bonuses) - first * sizeof(int) );
		memset(no_save.dmgmax_bonuses + first * sizeof(int), 0, sizeof(no_save.dmgmin_bonuses) - first * sizeof(int) );
		memset(no_save.speed_bonuses + first * sizeof(int), 0, sizeof(no_save.speed_bonuses) - first * sizeof(int) );
		memset(no_save.hp_bonuses + first * sizeof(int), 0, sizeof(no_save.hp_bonuses) - first * sizeof(int) );
		memset(no_save.shots_bonuses + first * sizeof(int), 0, sizeof(no_save.shots_bonuses) - first * sizeof(int) );
		memset(no_save.casts_bonuses + first * sizeof(int), 0, sizeof(no_save.casts_bonuses) - first * sizeof(int) );
		
		
		if (first == 0 && false ) {
			//memset(no_save.arteventtable_txt, 0, (NEW_ARTS_AMOUNT+1)*char_table_size);

			
			//memset(no_save.artname, 0, NEW_ARTS_AMOUNT*char_table_size);
			//memset(no_save.artdesc, 0, NEW_ARTS_AMOUNT*char_table_size);
			

		}
		else {
			//memset(save.ERM_Z_name + first * 2 * sizeof(int), 0, (sizeof(save.ERM_Z_name) / 2 - first * sizeof(int)) * 2);
		}
		
	}
*/


void __stdcall LoadConfigs(PEvent e)
{
	static bool firsttime = true;
	if (firsttime) {
		//blank(0);
	}
	else {
		//blank(OLD_ARTS_AMOUNT);
		//blank(NON_BLANK_ARTS_AMOUNT);
		//blank(0);
	}

   WIN32_FIND_DATAA ffd;
   HANDLE hFind = INVALID_HANDLE_VALUE;

   for(int i=0; i<NEW_ARTS_AMOUNT;i++)
   {
	   no_save.erm_on_ae0  [i][0] = '\0';
	   no_save.erm_on_ae1  [i][0] = '\0';
	   no_save.erm_on_ba52 [i][0] = '\0';
	   no_save.erm_on_ba53 [i][0] = '\0';
	   no_save.erm_on_timer[i][0] = '\0';
   }



   hFind = FindFirstFileA("data\\artifacts\\*.cfg", &ffd);
   /*
   hFind = FindFirstFileA(LPCSTR("Mods\\Amethyst Upgrades\\Data\\artifacts\\*.cfg"), &ffd);
   if (INVALID_HANDLE_VALUE == hFind)
	   hFind = FindFirstFileA(LPCSTR("Mods\\Knightmare Kingdoms\\Data\\artifacts\\*.cfg"), &ffd);
   if (INVALID_HANDLE_VALUE == hFind)
	   hFind = FindFirstFileA(LPCSTR("Data\\artifacts\\*.cfg"), &ffd);
	*/

   if (INVALID_HANDLE_VALUE == hFind)  {return;} 

   do
   {
      if (!(ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
      {
		 int artifact = atoi(ffd.cFileName);  //� �� �������� �� ��� atoi, � ��������� � ������?
		 if (artifact!=0)
		 {
			LoadArtifactConfig(artifact);
		 }
      }
   }
   while (FindNextFileA(hFind, &ffd) != 0);
   FindClose(hFind);
   firsttime = false;
}